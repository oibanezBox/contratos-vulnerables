webpackHotUpdate("main",{

/***/ "./src/components/CFP.js":
/*!*******************************!*\
  !*** ./src/components/CFP.js ***!
  \*******************************/
/*! exports provided: CFP */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CFP", function() { return CFP; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_sha256__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! js-sha256 */ "./node_modules/js-sha256/src/sha256.js");
/* harmony import */ var js_sha256__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_sha256__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! web3 */ "./node_modules/web3/lib/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _hooks_UseContract__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../hooks/UseContract */ "./src/hooks/UseContract.js");
/* harmony import */ var _hooks_UseContractCFP__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../hooks/UseContractCFP */ "./src/hooks/UseContractCFP.js");
/* harmony import */ var _resources_fox_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../resources/fox.svg */ "./src/resources/fox.svg");
/* harmony import */ var _resources_contract_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../resources/contract.svg */ "./src/resources/contract.svg");
/* harmony import */ var _ProposalContent__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ProposalContent */ "./src/components/ProposalContent.js");
/* harmony import */ var _CFP_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./CFP.css */ "./src/components/CFP.css");
/* harmony import */ var _CFP_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_CFP_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/home/oibanez/Escritorio/blockchain/ibanez/TP/7/src/components/CFP.js",
    _s = __webpack_require__.$Refresh$.signature();












const CFP = () => {
  _s();

  const [hash, setHash] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    "file": "",
    "name": "",
    "hash": "",
    "loading": true
  });
  const [web3, setWeb3] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const [netId, setNetId] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const [proposal, setProposal] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    window.$theme = 'light';

    const getNetId = async () => {
      const net = await window.ethereum.request({
        method: "net_version"
      });
      setNetId(net);
    };

    if (!!window.ethereum) {
      setWeb3(new web3__WEBPACK_IMPORTED_MODULE_2___default.a(window.ethereum));
      getNetId();
    }
  }, []);
  const [contract] = Object(_hooks_UseContract__WEBPACK_IMPORTED_MODULE_3__["UseContract"])({
    "networkId": netId
  });
  const [contractCFP] = Object(_hooks_UseContractCFP__WEBPACK_IMPORTED_MODULE_4__["UseContractCFP"])({
    "networkAdd": window.$callid
  });

  const handleChgFile = async e => {
    if (!!e.target.files[0]) {
      setHash({
        "file": e.target.files[0],
        "name": e.target.files[0].name,
        "hash": ""
      });
      let reader = new FileReader();
      reader.readAsArrayBuffer(e.target.files[0]);

      reader.onload = function (e) {
        const arrayBuffer = new Uint8Array(reader.result);
        const hs = js_sha256__WEBPACK_IMPORTED_MODULE_1___default.a.create();
        hs.update(arrayBuffer);
        setHash({ ...hash,
          "loading": false,
          "hash": `0x${hs.hex()}`
        });
        setProposal(null);
      };
    }
  };

  const handleRegister = async e => {
    e.preventDefault();
    const apiurl = "http://localhost:5000/register-proposal";
    const res = await fetch(apiurl, {
      method: "POST",
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        callId: window.$callid,
        proposal: hash.hash
      })
    });
    console.log(window.$callid);
    console.log(res);
    const json = await res.json();
    window.alert(json.message); // FALTA
  };

  const handleVerify = async e => {
    e.preventDefault();
    console.log(window.$callid);
    const apiurl = `http://localhost:5000/proposal-data/${window.$callid}/${hash.hash}`;
    const response = await fetch(apiurl, {
      method: "GET",
      mode: 'cors'
    });
    const data = await response.json();

    if (data.message) {
      setProposal(null);
      window.alert("La propuesta no fue registrada aún");
    } else {
      setProposal(data);
    }
  };

  const enableEth = async () => {
    const accounts = await window.ethereum.request({
      method: "eth_requestAccounts"
    });
    window.$account = accounts[0];
  };

  const handleMetaMask = async e => {
    e.preventDefault();

    if (!window.$account) {
      await enableEth();
    }

    console.log(window.$account);
    if (!window.$account) return;
    let res = await contractCFP.methods.proposalData(hash.hash).call();
    console.log(res);

    if (res.blockNumber != 0) {
      window.alert(`La propuesta, se registro previamente en el bloque Nro ${res.blockNumber}`);
    } else {
      if (window.$account && window.$callid && hash.hash) {
        console.log(contract);
        res = await contract.methods.registerProposal(window.$callid, hash.hash).send({
          from: window.$account
        });
        window.alert(`La propuesta, se registro con éxito! Bloque Nro ${res.blockNumber}`);
      } else {
        window.alert("Se produjo un error!");
      }
    }
  };

  window.ethereum.on('chainChanged', async networkVersion => {
    const getNetId = async () => {
      const net = await window.ethereum.request({
        method: "net_version"
      });
      setNetId(net);
    };

    getNetId();
  });
  window.ethereum.on('accountsChanged', accounts => {
    window.$account = accounts[0];
    console.log(window.$account);
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["Fragment"], {
    children: [!!web3 && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("img", {
      className: "animate__animated animate__fadeInDown",
      src: _resources_fox_svg__WEBPACK_IMPORTED_MODULE_5__["default"],
      alt: ""
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 154,
      columnNumber: 24
    }, undefined), !!contract && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("img", {
      className: "animate__animated animate__fadeInDown contract",
      src: _resources_contract_svg__WEBPACK_IMPORTED_MODULE_6__["default"],
      alt: ""
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 28
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("form", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("label", {
        for: "formFileLg",
        class: "form-label",
        children: "Selecione el archivo que contine la propuesta!"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 157,
        columnNumber: 17
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("input", {
        onChange: handleChgFile,
        className: "form-control archivo",
        id: "formFileLg",
        type: "file"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 158,
        columnNumber: 17
      }, undefined), !hash.loading && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("p", {
        children: ["Hash: ", hash.hash]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 159,
        columnNumber: 35
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("button", {
        disabled: hash.loading,
        className: "btn  btn-outline-success",
        onClick: handleRegister,
        children: "Registrar Propuesta "
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 160,
        columnNumber: 17
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("button", {
        disabled: hash.loading,
        className: "btn btn-outline-info",
        onClick: handleVerify,
        children: "Verificar Propuesta "
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 165,
        columnNumber: 17
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])("button", {
        disabled: hash.loading,
        hidden: !contract ? true : false,
        className: "btn btn-outline-danger",
        onClick: handleMetaMask,
        children: "Sellar con mi cuenta "
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 17
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 156,
      columnNumber: 13
    }, undefined), !!proposal && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__["jsxDEV"])(_ProposalContent__WEBPACK_IMPORTED_MODULE_7__["ProposalContent"], {
      data: proposal
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 28
    }, undefined)]
  }, void 0, true);
};

_s(CFP, "OmIXafp2UomM3SFHz81fKE2EqBk=");

_c = CFP;

var _c;

__webpack_require__.$Refresh$.register(_c, "CFP");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ })

})
//# sourceMappingURL=main.096721981231db322cb8.hot-update.js.map