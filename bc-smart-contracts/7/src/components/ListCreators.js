import React from 'react'
import { useGetCreators } from '../hooks/UseGetCreators';
import { TableCreators } from './TableCreators';

export const ListCreators = ({handleChgCont}) => {

    const [ response,data ] = useGetCreators();

    return (
        <div>
            <h2 className={window.$theme}>Creators List</h2>
            {!response.loading && <TableCreators data={data} handleChgCont={handleChgCont}/> }
        </div>
    )
}
