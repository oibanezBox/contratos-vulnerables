import React, { useState } from 'react'
import { useGetCall } from '../hooks/UseGetCall';
import { TableCalls } from './TableCalls';
import './ListContent.css'
export const ListCalls = ({handleChgCont}) => {
   
    const [creator] = useState(window.$creator);
    const [resp,data] = useGetCall();

    return (
        <>
            <h2 className={window.$theme}>List of calls from creator: {creator}</h2>
            {!resp.loading && <TableCalls handleChgCont={handleChgCont} data={data}/>}
        </>
    )
}
