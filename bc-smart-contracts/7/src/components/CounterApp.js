import React from 'react';
import PropTypes from  'prop-types'

export default function CounterApp(props){


    return(
        <>
            <h1>Counter app</h1>
            <h2>{ props.value }</h2>
        </>
    )
}

CounterApp.PropsType = {
    value: PropTypes.number
}