import React from 'react'

export const TableCreators = ({data,handleChgCont}) => {

    const handleClick = (e) => {
        window.$creator = e.target.innerHTML
        handleChgCont("callsTable")
    }

    return (
        <>
        <div className='box animate__animated animate__backInLeft animate__slow'>
            <table class={"table"+window.$theme}>
                <thead>
                    <tr>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map( creator => {
                            return (<tr onClick={handleClick}>
                                <td>{creator}</td>
                            </tr>)
                        }  
                    )}
                </tbody>
            </table>
        </div>
    </>
    )
}
