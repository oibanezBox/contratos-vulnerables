import React, {useState} from 'react';
import tsaLogo from '../resources/tsa.png'
import './NavBar.css'

export default function NavBar(props){
    const [theme, setTheme] = useState("light");
    
    //Styled components --> button 

    // const ThemeButton = styled.button`
    //     border-color: lightgray;
    //     padding: 1rem;
    //     color: ${theme === 'dark'? "white":"black"};
    //     background-color: ${theme === 'dark'? "black":"white"};
    // `
    // console.log(ThemeButton)
    // console.log(theme)

    return(
        <>
            <nav className={"navbar navbar-expand-lg navbar-"+theme+" bg-"+theme}>
                <div className="container-fluid">
                    {/* <a className="navbar-brand" href="#">Box Custodia</a> */}
                    <img onClick={ (e) => props.login(false)} className="ico" alt="" src={tsaLogo}></img>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {/* <li className="nav-item">
                        <a  className="nav-link active" aria-current="page" href="#">TSA</a>
                        </li> */}

                        <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Menu
                        </a>
                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a onClick={(e) => props.handleChgCont("creatorsTable")}className="dropdown-item" href="#">Home</a></li>
                            <li><a onClick={(e)=>props.login(true)} className="dropdown-item" href="#">Login</a></li>
                            <li><hr className="dropdown-divider"/></li>
                            <li><a className="dropdown-item" href="#">FAQS</a></li>
                        </ul>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Administrar</a>
                        </li>
                    </ul>
                    <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                        <button disabled={true} className="btn btn-outline-success" type="submit">Search</button>
                        <button className={"btn btn-outline btn-theme-"+theme} onClick={
                            (e) => {
                                e.preventDefault();    
                                theme === "dark"? setTheme("light") : setTheme("dark");
                                props.onChangeTheme(theme);
                                window.$theme = theme
                            }
                        }>Theme</button>
                        {/* <ThemeButton theme={theme} onClick={ (e) => {e.preventDefault();
                            theme === "light"? setTheme("dark") : setTheme("light");}}>Theme</ThemeButton> */}
                    </form>
                    </div>
                </div>
            </nav>
        </>
    )
}