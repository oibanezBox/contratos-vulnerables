import React, { useEffect, useState } from 'react'
import sha256 from 'js-sha256'
import Web3 from 'web3'
import { UseContract } from '../hooks/UseContract'
import { UseContractCFP } from '../hooks/UseContractCFP'
import svg from '../resources/fox.svg'
import cont from '../resources/contract.svg'
import { ProposalContent } from './ProposalContent'
import './CFP.css'
export const CFP = () => {
    
    const [hash,setHash] = useState({
        "file":"",
        "name":"",
        "hash": "",
        "loading": true
    })
    const [web3, setWeb3] = useState(null)
    const [netId,setNetId] = useState("")
    const [proposal, setProposal] = useState(null);

    useEffect(() => {
        //window.$theme = 'dark'
        
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetId(net)
            }
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
            }
        }, [])
        
        
        const [contract] = UseContract({
            "networkId": netId,
        })

        const [contractCFP] = UseContractCFP({
        "networkAdd": window.$callid
    })
        
    const handleChgFile = async (e)=>{
            if(!!e.target.files[0]){
            setHash({
            "file":e.target.files[0],
            "name":e.target.files[0].name,
            "hash": ""
            })   
            let reader = new FileReader();
            
            reader.readAsArrayBuffer(e.target.files[0])
            reader.onload = function(e) {
                
                const arrayBuffer = new Uint8Array(reader.result);
                const hs =sha256.create()
                hs.update(arrayBuffer)
                setHash({
                    ...hash,
                    "loading": false,
                    "hash": `0x${hs.hex()}`
                })
                setProposal(null)
            }   
        }
    }

    const handleRegister = async (e) =>{
        e.preventDefault()
        const apiurl = "http://localhost:5000/register-proposal"
        const res = await fetch(apiurl, {
            method: "POST", 
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify({callId: window.$callid, proposal: hash.hash})
        })
        console.log(window.$callid)
        console.log(res)
        const json = await res.json()
        window.alert(json.message)
        // FALTA
    }

    const handleVerify = async (e) => {
        e.preventDefault()
        console.log(window.$callid)
        const apiurl = `http://localhost:5000/proposal-data/${window.$callid}/${hash.hash}`
        const response = await fetch(apiurl,{
            method: "GET", 
            mode: 'cors'
        })

        const data = await response.json()
        if(data.message){
            setProposal(null)
            window.alert("La propuesta no fue registrada aún")
        }else{
            setProposal(data)
        }
        
    }


    const enableEth = async () => {
        const accounts = await window.ethereum.request({method: "eth_requestAccounts"})
        window.$account = accounts[0]
    }

    const handleMetaMask = async (e) => {
        e.preventDefault()
        if(!window.$account){
            await enableEth()
        }

        if(!window.$account) return
        let res = await contractCFP.methods.proposalData(hash.hash).call()
        if(res.blockNumber !== "0"){
            window.alert(`La propuesta, se registro previamente en el bloque Nro ${res.blockNumber}`)
        }else{
            if(window.$account && window.$callid && hash.hash){
                console.log(contract)
                res = await contract.methods.registerProposal(window.$callid,hash.hash).send({from: window.$account})
                window.alert(`La propuesta, se registro con éxito! Bloque Nro ${res.blockNumber}`)
            }else{
                window.alert("Se produjo un error!")
            }
        }
    }

    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
            console.log(window.$account)
        });
    }

    return (
        <>
            <div className={"box"+window.$theme}>
                {!!web3 && <img className="animate__animated animate__fadeInDown animate__slow" src={svg} alt=""></img>}
                {!!contract && <img className="animate__animated animate__fadeInDown animate__slow contract" src={cont} alt=""></img>}
                <form>
                    <label for="formFileLg" class="form-label animate__animated animate__fadeInDown animate__slow">Selecione el archivo que contine la propuesta!</label>
                    <div className="archivo">
                    <input onChange={handleChgFile} className="form-control archivoInput animate__animated animate__fadeInDown animate__slow" id="formFileLg" type="file"/>
                    </div>
                    {!hash.loading && <p>Hash: {hash.hash}</p>}
                    <button
                    disabled={hash.loading}
                    className="btn  btn-outline-success customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleRegister}
                    >Registrar Propuesta </button>   
                    <button
                    disabled={hash.loading}
                    className="btn btn-outline-info customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleVerify}
                    >Verificar Propuesta </button>   
                    <button 
                    disabled={hash.loading}
                    hidden={!contract? true : false}
                    className="btn btn-outline-danger customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleMetaMask}
                    >Sellar con mi cuenta </button>   
                </form>
            </div>
                {!!proposal && <ProposalContent data={proposal}></ProposalContent>}  
        </>
    )
}
