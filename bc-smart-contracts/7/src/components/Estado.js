import React, { Component } from 'react'

let interval = 1000


function EstadoHijo(props){
    return (
           <p>{props.contadorHijo}</p>
    )
}

export default class Estado extends Component{

    constructor(props){
        // Le paso las propiedades al constructor de la clase Component
        super(props)
        // Cuando cambia el estado se renderiza el elemnto que tiene este objeto
        this.state = {
            contador : 0
        }

        setInterval(() => {
           this.setState( {
               contador: this.state.contador + 1
           }) 
        }, interval);
    }

    

    render(){
        return ( 
            <section>
                <p><h4>Contador</h4></p>
                <div><p>{this.state.contador}</p></div>
                <div>
                    <p>Componente hijo contador</p>
                    <EstadoHijo contadorHijo={this.state.contador}></EstadoHijo>
                </div>
            </section>
        )
    }

}