//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract CFP {
    // Evento que se emite cuando alguien registra una propuesta
    event ProposalRegistered(
        bytes32 proposal,
        address sender,
        uint256 blockNumber
    );

    // Estructura que representa una propuesta
    struct ProposalData {
        address sender;
        uint256 blockNumber;
        uint256 timestamp;
    }

    // Devuelve los datos asociados con una propuesta
    // function proposalData(bytes32 proposal)
    //     public
    //     view
    //     returns (ProposalData memory)
    // {}

    address public creator;
    uint256 public closingTime;
    bytes32 public callId;

    mapping(bytes32 => ProposalData) public proposalData;

    // Arreglo dinámico de datos de propuestas.
    bytes32[] public proposalsAd;

    // Devuelve la propuesta que está en la posición `index` de la lista de propuestas registradas
    function proposals(uint index) public view returns (bytes32) {
        return proposalsAd[index];
    }

    // Timestamp del cierre de la recepción de propuestas
    //function closingTime() public view returns (uint256) {}

    // Identificador de este llamado
    //function callId() public view returns (bytes32) {}

    // Creador de este llamado
    //function creator() public view returns (address) {}

    /** Construye un llamado con un identificador y un tiempo de cierre.
     *  Si el `timestamp` del bloque actual es mayor o igual al tiempo de cierre especificado,
     *  revierte con el mensaje "El cierre de la convocatoria no puede estar en el pasado".
     */
    constructor(bytes32 _callId, uint256 _closingTime) {
        require(block.timestamp < _closingTime,"El cierre de la convocatoria no puede estar en el pasado");
        creator = msg.sender;
        callId = _callId;
        closingTime = _closingTime;
    }

    // Devuelve la cantidad de propuestas presentadas
    function proposalCount() public view returns (uint256) {
        return proposalsAd.length;
    }

    /** Permite registrar una propuesta espec.
     *  Registra al emisor del mensaje como emisor de la propuesta.
     *  Si el timestamp del bloque actual es mayor que el del cierre del llamado,
     *  revierte con el error "Convocatoria cerrada"
     *  Si ya se ha registrado una propuesta igual, revierte con el mensaje
     *  "La propuesta ya ha sido registrada"
     *  Emite el evento `ProposalRegistered`
     */
    function registerProposal(bytes32 proposal) public inTime notRegistered(proposal){ 
        proposalData[proposal].sender = msg.sender;
        proposalData[proposal].blockNumber = block.number;
        proposalData[proposal].timestamp = block.timestamp;
        proposalsAd.push(proposal);
        emit ProposalRegistered(proposal, msg.sender, block.number);
    }

    /** Permite registrar una propuesta especificando un emisor.
     *  Sólo puede ser ejecutada por el creador del llamado. Si no es así, revierte
     *  con el mensaje "Solo el creador puede hacer esta llamada"
     *  Si el timestamp del bloque actual es mayor que el del cierre del llamado,
     *  revierte con el error "Convocatoria cerrada"
     *  Si ya se ha registrado una propuesta igual, revierte con el mensaje
     *  "La propuesta ya ha sido registrada"
     *  Emite el evento `ProposalRegistered`
     */
    function registerProposalFor(bytes32 proposal, address sender) public inTime notRegistered(proposal) onlyCreator{
        proposalData[proposal].sender = sender;
        proposalData[proposal].blockNumber = block.number;
        proposalData[proposal].timestamp = block.timestamp;
        proposalsAd.push(proposal);
        emit ProposalRegistered(proposal, sender, block.number);
    }

    /** Devuelve el timestamp en el que se ha registrado una propuesta.
     *  Si la propuesta no está registrada, devuelve cero.
     */
    function proposalTimestamp(bytes32 proposal)
        public
        view
        returns (uint256)
    {
        if(proposalData[proposal].blockNumber > 0){
            return proposalData[proposal].timestamp;
        }else{
            return 0;
        }
    }

    modifier inTime() {
        require(closingTime > block.timestamp,"Convocatoria cerrada");
        _;
    }

    modifier notRegistered(bytes32 proposal) {
        require(proposalData[proposal].blockNumber == 0,"La propuesta ya ha sido registrada");
        _;
    }

    modifier onlyCreator() {
        require(
            msg.sender == creator,
            "Solo el creador puede hacer esta llamada"
        );
        _;
    }
    
}
