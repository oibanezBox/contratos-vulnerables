import { useEffect, useState } from 'react'
import Web3 from 'web3'

export const UseContractCFP = ({networkAdd}) => {

    const [contract, setContract] = useState(null);
    
    const [response,setResponse] = useState({
        "loading": true
    });
    
    useEffect(() => {
        const web3 = window.ethereum? new Web3(window.ethereum):undefined
        const dataC = require("../ethereum/build/contracts/CFP.json")
        let addressCon = null
        
        const getData = async ()=> {
            const apiUrl = `http://localhost:5000/calls/${networkAdd}`

            try{
                const res = await fetch(apiUrl,{
                    method: "GET", 
                    mode: 'cors'})
                

                const response = await res.json()
                const {message} = response
                    
                if(message){
                    setResponse({
                        "err":false,
                        "message": message,
                        "loading": false
                    })
                    window.alert(message)
                }else{
                    setResponse({
                        ...response,
                        "message": "ok",
                        "loading": false
                    })
                    addressCon = response.cfp
                }
            }catch(err){
                console.log(err)
                }
            
        }
        const getContract = async () =>{
            if(web3){
                await getData()
                const address = addressCon
                if(address){
                    setContract(new web3.eth.Contract(dataC.abi,address))
                }
            }
        }
       
        getContract()
    }, [networkAdd,setResponse])

    return [contract,response]
}
