import { useState } from "react"


export const UseContent = () => {
    
    const [content, setContent] = useState(
        {
            "creatorsTable": true,
            "callsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false
        }
    );

    const handleChangeContent = ( contentToChg ) => {
        
        if( contentToChg === "callsTable") setContent({
            "callsTable": true,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false
        })

        if( contentToChg === "creatorsTable") setContent({
            "creatorsTable": true,
            "callsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false
        })

        if( contentToChg === "cfp") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": true,
            "cfpRegister": false,
            "cfpVerify": false
        })
        if( contentToChg === "cfpRegister") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": true,
            "cfpVerify": false
        })
        if( contentToChg === "cfpVerify") setContent({
            "callsTable": true,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": true
        })
    }


    return [content, handleChangeContent]
    
}
