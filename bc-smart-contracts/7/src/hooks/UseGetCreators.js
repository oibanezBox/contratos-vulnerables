import { useEffect, useState } from 'react'

export const useGetCreators = () => {

    const [response, setResponse] = useState({
        "loading": true
    });
 
    const [data, setData] = useState([]);

    useEffect(() => {
        const getResponse = async ()=>{
            const apiUrl=`http://localhost:5000/creators`
        
            try{
                const res = await fetch(apiUrl,{
                    method: "GET", 
                    mode: 'cors'})
                

                const response = await res.json()
                const {message} = response
                    
                if(message){
                    setResponse({
                        "err":false,
                        "message": message,
                        "loading": false
                    })
                    window.alert(message)
                }else{
                    setResponse({
                        ...response,
                        "message": "ok",
                        "loading": false
                    })
                    console.log(response)
                    setData(response.creators)
                    }
                }catch(err){
                    console.log(err)
                    }
                
                }
                getResponse()
            }
        , [])

    return [response,data]
}
