import { useState } from "react";


export const useInputValue = ( initialValue = "") => {

    const [input, setInput] = useState(initialValue);

    const handleInputChange = (e) =>{
        setInput(e.target.value)
    }

    return [input, handleInputChange, setInput]
}
