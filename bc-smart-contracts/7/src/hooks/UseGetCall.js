import  { useEffect, useState } from 'react'

export const useGetCall = () => {

    const [response, setResponse] = useState({
        "loading": true
    });
    const [address] = useState(window.$creator);
    const [data, setData] = useState([]);

    useEffect(() => {
        const getResponse =async ()=>{
            const apiUrl=`http://localhost:5000/creators/${address}`
                try{
                        const res = await fetch(apiUrl,{
                            method: "GET", 
                            mode: 'cors'})
                        

                        const response = await res.json()
                        const {message} = response
                            
                        if(message){
                            setResponse({
                                "err":false,
                                "message": message,
                                "loading": false
                            })
                            window.alert(message)
                        }else{
                            setResponse({
                                ...response,
                                "message": "ok",
                                "loading": false
                            })
                            console.log(response)
                            setData(response.creatorCalls)
                        }
                    }catch(err){
                        console.log(err)
                        }
                    }
                    getResponse()
                }
            
        , [address])

    return [response,data]
}
