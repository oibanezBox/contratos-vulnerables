import { useEffect, useState } from "react";
import {CryptoJS} from 'crypto-js';

export const UseHashFile = (input) => {
    const [hash, setHash] = useState();

    useEffect(() => {
        
        const reader = new FileReader();

        reader.onloadend = function(evt) {
        if (evt.target.readyState == FileReader.DONE) { 
            const wordArray = CryptoJS.lib.WordArray.create(input.target.result);
            setHash(CryptoJS.SHA256(wordArray));
        }
     };
       
    }, [input])
    

     return [hash,setHash];
}
