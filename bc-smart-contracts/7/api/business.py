import constants
import re
from flask import json, jsonify
import dateutil.parser
from datetime import timezone
import datetime
import traceback
from web3.exceptions import ValidationError
from eth_account import Account
from eth_account.messages import encode_defunct
hash_pattern = re.compile(r"0x[0-9a-fA-F]{64}")
hash_pubKey = re.compile(r"0x[0-9a-fA-F]{40}")
signature_pattern = re.compile(r"0x[0-9a-fA-F]{130}")
db = {}
zeroAddress = "0x0000000000000000000000000000000000000000"
class CFPBusiness():

    def __init__(self,w3,contract,mnemonic,abi):
        self.w3 = w3
        self.contract = contract
        self.mnemonic = mnemonic
        Account.enable_unaudited_hdwallet_features()
        self.account = Account.from_mnemonic(mnemonic)
        self.private_key = self.account.privateKey
        self.abi = abi


    def create(self,callId,closingTime,signature):
        try:
            dateSeconds = dateutil.parser.parse(closingTime)
            dateSeconds = dateSeconds.timestamp()
            dateTime = datetime.datetime.now(timezone.utc)
            utc_time = dateTime.replace(tzinfo=timezone.utc)
            currentTime = utc_time.timestamp()
            if(currentTime > dateSeconds):
                response = jsonify(message= constants.INVALID_CLOSING_TIME)
                response.status_code = 400
                return response
            if not self.is_valid_hash(callId):
                response= jsonify(message= constants.INVALID_CALLID)
                response.status_code = 400
                return response
            if not self.is_valid_signature(signature):
                response= jsonify(message= constants.INVALID_SIGNATURE)
                response.status_code = 400
                return response
            else:
                message = encode_defunct(hexstr=callId)
                pubKey = self.w3.eth.account.recover_message(message,signature = signature)    
            responseContract = self.contract.functions.calls(callId).call()
            if responseContract[0] != zeroAddress:
                response= jsonify(message= constants.ALREADY_CREATED)
                response.status_code = 403
                return response
            responseContract = self.contract.functions.isAuthorized(pubKey).call()
            if responseContract == False:
                response= jsonify(message= constants.UNAUTHORIZED)
                response.status_code = 403
                return response
            #Logica para crear el llamado desde el contrato // Revisar lo del gas
            nonce = self.w3.eth.getTransactionCount(self.account.address)
            transaction = self.contract.functions.createFor(callId,int(dateSeconds),pubKey).buildTransaction({'gas': 2000000,'nonce': nonce})
            #
            signed = self.w3.eth.account.sign_transaction(transaction,self.private_key)
            hexHash = self.w3.eth.send_raw_transaction(signed.rawTransaction)
            result = self.w3.eth.waitForTransactionReceipt(hexHash)
            if(result['status'] == 1):
                response = jsonify(message = constants.OK)
                response.status_code = 201
            else:
                response = jsonify(message = constants.INTERNAL_ERROR)
                response.status_code = 500


        except ValueError as e:
            response = jsonify(message= constants.INVALID_TIME_FORMAT)
            response.status_code = 400
            if(str(e)[0] == '{'):
                error = json.loads(str(e).replace("'","\""))
                error = str(error['data']).split("revert ")[1]
                if("No autorizado" in error):
                    response = jsonify(message= constants.UNAUTHORIZED)
                    response.status_code = 400
                    return response
                if("El llamado ya existe" in error):
                    response = jsonify(message= constants.ALREADY_CREATED)
                    response.status_code = 400
                    return response
                if("El cierre de la convocatoria no puede estar en el pasado" in error):
                    response = jsonify(message= constants.INVALID_CLOSING_TIME)
                    response.status_code = 400
                    return response
                else:
                    response = jsonify(message= constants.INTERNAL_ERROR)
                    response.status_code = 500
                    traceback.print_exc()
                    return response
            
        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            traceback.print_exc()
            return response
        
        return response


    def register(self,address, signature):
        print("LLamó al metodo register para autorizar")
        try:
            if not self.is_valid_address(address):
                response = jsonify(message= constants.INVALID_ADDRESS)
                response.status_code = 400
                return response
            account = self.w3.toChecksumAddress(address)
            message = encode_defunct(hexstr=address)
            if not self.is_valid_signature_address(account,message,signature):
                response = jsonify(message= constants.INVALID_SIGNATURE)
                response.status_code = 400
                return response
            responseContract = self.contract.functions.isAuthorized(account).call()
            if responseContract == True:
                response = jsonify(message= constants.ALREADY_AUTHORIZED)
                response.status_code = 403
                return response
             #Logica para crear el llamado desde el contrato
            nonce = self.w3.eth.getTransactionCount(self.account.address)
            transaction = self.contract.functions.authorize(account).buildTransaction({'gas': 200000,'nonce': nonce})
            #
            signed = self.w3.eth.account.sign_transaction(transaction,self.private_key)
            hexHash = self.w3.eth.send_raw_transaction(signed.rawTransaction)
            result = self.w3.eth.waitForTransactionReceipt(hexHash)
            if(result['status'] == 1):
                response = jsonify(message = constants.OK)
                response.status_code = 200
            else:
                response = jsonify(message = constants.INTERNAL_ERROR)
                response.status_code = 213

        except ValueError as e:
            response = jsonify(message= constants.INVALID_ADDRESS)
            response.status_code = 400
            return response

        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            traceback.print_exc()
            return response
        
        return response

    def registerProposal(self,callId, proposal):
        print(callId)
        print(proposal)
        try:
            if not self.is_valid_hash(callId):
                response= jsonify(message= constants.INVALID_CALLID)
                response.status_code = 400
                return response
            if not self.is_valid_hash(proposal):
                response= jsonify(message= constants.INVALID_PROPOSAL)
                response.status_code = 400
                return response
            responseContract = self.contract.functions.calls(callId).call()
            if responseContract[0] == zeroAddress:
                response= jsonify(message= constants.CALLID_NOT_FOUND)
                response.status_code = 404
                return response
            contractCFP =  self.w3.eth.contract( abi = self.abi, address = responseContract[1] )
            responseContract = contractCFP.functions.proposalData(proposal).call()
            if(responseContract[2] != 0):
                response= jsonify(message= constants.ALREADY_REGISTERED)
                response.status_code = 403
                return response
            #Logica para crear el llamado desde el contrato
            nonce = self.w3.eth.getTransactionCount(self.account.address)
            transaction = self.contract.functions.registerProposal(callId,proposal).buildTransaction({'gas': 800000,'nonce': nonce})
            #
            signed = self.w3.eth.account.sign_transaction(transaction,self.private_key)
            hexHash = self.w3.eth.send_raw_transaction(signed.rawTransaction)
            result = self.w3.eth.waitForTransactionReceipt(hexHash)
            if(result['status'] == 1):
                response = jsonify(message = constants.OK)
                response.status_code = 201
            else:
                response = jsonify(message = constants.INTERNAL_ERROR)
                response.status_code = 500
            
        except ValueError as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            if(str(e)[0] == '{'):
                error = json.loads(str(e).replace("'","\""))
                error = str(error['data']).split("revert ")[1]
                if("El llamado no existe" in error):
                    response= jsonify(message= constants.CALLID_NOT_FOUND)
                    response.status_code = 404
                    return response
                if("Convocatoria cerrada" in error):
                    response= jsonify(message= constants.PROPOSAL_CLOSED)
                    response.status_code = 400
                    return response
                if("La propuesta ya ha sido registrada" in error):
                    response= jsonify(message= constants.ALREADY_REGISTERED)
                    response.status_code = 403
                    return response
            return response
        

        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            return response
        return response
    
    def isAuthorized(self,address):
        try:
            if not self.is_valid_address(address) :
                response = jsonify(message= constants.INVALID_ADDRESS)
                response.status_code = 400
                return response
            else:
                account = self.w3.toChecksumAddress(address)
                isAuth = self.contract.functions.isAuthorized(account).call()
                response = jsonify(authorized = isAuth)
                response.status_code = 200
                return response
        except ValueError as e:
            response = jsonify(message= constants.INVALID_ADDRESS)
            response.status_code = 400
            return response
        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            traceback.print_exc()
            return response


    def calls(self,callid):
        try:
            if not self.is_valid_hash(callid):
                response= jsonify(message= constants.INVALID_CALLID)
                response.status_code = 400
                return response
            responseContract = self.contract.functions.calls(callid).call()
            if responseContract[0] == zeroAddress:
                response= jsonify(message= constants.CALLID_NOT_FOUND)
                response.status_code = 404
                return response
            response= jsonify(creator= responseContract[0],cfp= responseContract[1])
            response.status_code = 200
            return response

        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            return response

    def closingTime(self,callid):
        try:
            if not self.is_valid_hash(callid):
                response= jsonify(message= constants.INVALID_CALLID)
                response.status_code = 400
                return response
            responseContract = self.contract.functions.calls(callid).call()
            if responseContract[0] == zeroAddress:
                response= jsonify(message= constants.CALLID_NOT_FOUND)
                response.status_code = 404
                return response
            contractCFP =  self.w3.eth.contract( abi = self.abi, address = responseContract[1] )
            responseContract = contractCFP.functions.closingTime().call()
            if responseContract != 0:
                date = datetime.datetime.fromtimestamp(responseContract,tz=timezone.utc).isoformat()
                response= jsonify(closingTime= date)
                response.status_code = 200
                return response
        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            return response

    def creators(self):
        creators = []
        creatorsCount = self.contract.functions.creatorsCount().call()
        for i in range(creatorsCount):
            address = self.contract.functions.creators(i).call()
            creators.append(address)
        response = jsonify(creators = creators)
        response.status_code = 200
        return response


    def callsFromCreator(self,creator):
        if not self.is_valid_address(creator):
            response= jsonify(message= constants.INVALID_ADDRESS)
            response.status_code = 400
            return response
        creatorCalls = []
        try:
            creatorCallCount = self.contract.functions.createdByCount(creator).call()
            print(creatorCallCount)
            for i in range(creatorCallCount):
                address = self.contract.functions.createdBy(creator,i).call()
                call = self.w3.toHex(address)
                creatorCalls.append(call)
            response = jsonify(creatorCalls = creatorCalls)
            response.status_code = 200
            return response
        except ValidationError as e:
            response= jsonify(message= constants.CREATOR_NOT_FOUND)
            response.status_code = 400
            return response


    def contractAddress(self):
        response = jsonify(address= self.contract.address)
        response.status_code = 200
        return response
    
    def contractOwner(self):
        response = jsonify(address= self.contract.functions.owner().call())
        response.status_code = 200
        return response

    def proposalData(self,callid,proposal):
        try:
            if not self.is_valid_hash(callid):
                response= jsonify(message= constants.INVALID_CALLID)
                response.status_code = 400
                return response
            responseContract = self.contract.functions.calls(callid).call()
            if responseContract[0] == zeroAddress:
                response= jsonify(message= constants.CALLID_NOT_FOUND)
                response.status_code = 404
                return response
            if not self.is_valid_hash(proposal):
                response= jsonify(message= constants.INVALID_PROPOSAL)
                response.status_code = 400
                return response
            contractCFP =  self.w3.eth.contract( abi = self.abi, address = responseContract[1] )
            responseContract = contractCFP.functions.proposalData(proposal).call()
            if(responseContract[2] == 0):
                response= jsonify(message= constants.PROPOSAL_NOT_FOUND)
                response.status_code = 404
                return response
            date = datetime.datetime.utcfromtimestamp(responseContract[2]).isoformat()
            response = jsonify(sender = responseContract[0],blockNumber= responseContract[1],timestamp = date)
            response.status_code = 200
            return response
        except ValueError as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            return response    
        except Exception as e:
            response = jsonify(message= constants.INTERNAL_ERROR)
            response.status_code = 500
            traceback.print_exc()
            return response

    def is_valid_hash(self,h):
        return re.match(hash_pattern, h)

    def is_valid_address(self,h):
        return re.match(hash_pubKey, h)    


    def is_valid_signature(self,h):
        return re.match(signature_pattern, h)
        # if h[-2:] =="1b" or h[-2:] =="1c":
        #     return True
        # else:
        #     return False
    def is_valid_signature_address(self,address,message,signature):
        if address == self.w3.eth.account.recover_message(message,signature = signature):
            return True
        else:
            return False