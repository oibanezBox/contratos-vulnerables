Hola profe estas son algunas observaciones a tener en cuenta:

Debido a una limitación propia de react js, los contratos tuve que dejarlos dentro de la carpeta src, ya que si están fuera de la misma estos no pueden ser accedidos por react js.

Dentro de la carpeta del proyecto va a encontrar la api que puede utilizar (realmente da lo mismo que use esta o la del tp 6).

Respecto al codigo se que no es la mejor implementación ya que soy un novato con react, pero creo que se defiende bien, también hay algunas cosas pensadas para un futuro las cuales no son completamente funcionales (search de la barra y el login el cual si lo muestra puede oculatarlo haciendo click en el logo de la página)

Adiciones para el Final

Hola profe, le comento que modfiqué para el final, primero comentarle que tuve que modificar la api le agregué los siguientes enpoints:

/pendingRegister/(address)/(page) --> GET

Descripción:
    Me permite obtener todos los registros pendientes para ser autorizados. En este endpoint implemente un sistema de paginado muy básico, pero al final por falta de tiempo no lo termine aplicando en el front-end.

Posibles respuestas:

    200 --> OK
    400 --> BAD_REQUEST
    403 --> UNAUTHORIZED
    500 --> INTERNAL_SERVER_ERROR

/authorize --> POST

Descripción:
    Me permite autorizar a las cuentas pendientes.

Posibles respuestas:

    200 --> OK
    400 --> BAD_REQUEST
    403 --> UNAUTHORIZED
    500 --> INTERNAL_SERVER_ERROR

/calls/isRegister/(callid)

Descripción:
    Me permite saber rápidamente si un llamado fue registrado con ese callid. Devuelve un booleano, true si está registrado, false en caso de no estarlo.

Posibles respuestas:

    200 --> OK
    400 --> BAD_REQUEST
    403 --> UNAUTHORIZED
    500 --> INTERNAL_SERVER_ERROR


Respecto al front-end se trabajó en muchas mejores principalmente en la parte estética, ahora las notificaciones son más acordes con la ui (no uso más window.alert), además para cumplir con lo requerido se implementó en el home, dos secciones que se habilitan solo si tiene metamask instalado en el navegador, estas son: 

my account → Permite registrar por única vez el nombre de su cuenta, obviamente no puede registrar uno ya elegido por otro usuario.

my calls → Permite crear llamados, solo si primero registró un nombre para su cuenta, y en una primera instancia deberá pasar la etapa de registro para que sea autorizado y pueda crear llamados. Una vez autorizado podrá crear llamados muy fácilmente, eligiendo una fecha de cierre válida, un nombre y una descripción. Se especifica en cada paso que es lo que se está pagando.

También podrá encontrar un panel de administración en la barra de navegación este panel sólo es accesible por la cuenta creadora (en mi caso la cuenta 0 de mi red ganache), en este panel se listaran las cuentas registradas pendientes a ser autorizadas. Puede seleccionar una o varias marcando las casillas, como usted guste.

Finalmente respecto al menú de los creadores y los calls, detecta si tiene meta mask y en base a ello muestra distinta información, si tiene instalado meta mask y está en la red donde desplegó los contratos entonces podrá ver los nombres de usuarios y los nombres de los llamados, en caso contrario solo verá direcciones hexadecimales por lo que se recomienda usar meta mask.

Olvide mencionar que modifique los contratos de resolución inversa, en realidad solo el que se encarga de los llamados, para obtener la funcionalidad que deseaba. 

Espero que sea de su agrado. Saludos!

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
