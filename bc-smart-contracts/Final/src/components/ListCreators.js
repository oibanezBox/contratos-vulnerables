import React, { useEffect, useState } from 'react'
import { useGetAccountNames } from '../hooks/UseGetAccountNames';
import { useGetCreators } from '../hooks/UseGetCreators';
import { TableCreators } from './TableCreators';
import Web3 from 'web3'
import { UseContractGeneric } from '../hooks/UseContractGeneric';
import { TableCreatorsENS } from './TableCreatorsENS';

export const ListCreators = ({handleChgCont}) => {

    const [ response,data ] = useGetCreators();
    const [metamask, setMetamask] = useState(null)
    const [web3, setWeb3] = useState(null)
    const [networkId,setNetworkId] = useState("")
    const namehash = require("eth-ens-namehash")


    useEffect(() => {
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
                
            }
            
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
                setMetamask(true)
            }
        
    }, [])

    const [contractResolver] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "PublicResolverAccounts.json"
    });

    const [ responseNames, dataAccountNames ] = useGetAccountNames(data,contractResolver);

    const getList = () => {
        const array = []
        if(metamask){
            for(let i = 0; i<data.length; i++){
                array.push({address: data[i], accountName: dataAccountNames[i]})
            }
        }
        return array
    }

    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
        });
    }

    return (
        <div>
            <h2 className={window.$theme}>Creators List</h2>
            {!response.loading && contractResolver==null && <TableCreators data={data} handleChgCont={handleChgCont}/> }
            {!response.loading && !!contractResolver && metamask && !responseNames && <TableCreatorsENS data={getList} accountNames={dataAccountNames} handleChgCont={handleChgCont}/>} 
        </div>
    )
}
