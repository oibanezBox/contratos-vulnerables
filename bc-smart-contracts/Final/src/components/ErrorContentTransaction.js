
import React from 'react'
import cross from '../resources/cross.svg'
import './info.css'
export const ErrorContentTransaction = () => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div className="alert alert-danger " role="alert">
                
                <h4 className="alert-heading"><img className="info" src={cross}></img> Transacción fallida!</h4>
                <p>La transacción que estaba pendiente fue rechazada o falló, reintentelo nuevamente</p>
                    <hr/>
                <p className="mb-0">Tambien puede ponerse en contacto con el administrador.</p>
            </div>
        </div>
    )
}
