import React, { useState } from 'react'

export const RegisterNameCard = ({handleCheckIsAvailable,handleRegistry}) => {

    const [name, setName] = useState("")
    const [okName, setOkName] = useState(false)


    const handleChange = async (e) => {
        
        setName(e.target.value)
        setOkName( await handleCheckIsAvailable(e.target.value))
        
    }

    return (
    <div className="box animate__animated animate__backInLeft animate__slow">
        <div class={"card"+window.$theme}>
            <div class="card-body">
                <h4 class="card-title">Registro de nombre</h4>
                <p class="card-text mt-2 cup">Ingrese un nombre para su cuenta!</p>
                <input type="text" onChange={ (e) => handleChange(e)} value={name}></input>
                <button disabled={!okName} onClick={ (e) => handleRegistry(name) } className="btn-outline-success mt-2">Registrar!</button>
            </div>
        </div>
    </div>
    )
}
