import React from 'react'

export const TableCalls = ({data,handleChgCont}) => {

    const handleClick = (e) => { 
        window.$callid = e.target.parentElement.children[0].innerHTML
        handleChgCont("cfp")
    }

    return (
        <>
            <div className='box animate__animated animate__backInLeft animate__slow'>
                <table class={"table"+window.$theme}>
                    <thead>
                        <tr>
                            <th scope="col">Call Id</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map( call => {

                                return (<tr onClick={handleClick}>
                                    <td>{call}</td>
                                </tr>)
                                
                            }  
                        )}
                    </tbody>
                </table>
            </div>
        </>
    )
}
