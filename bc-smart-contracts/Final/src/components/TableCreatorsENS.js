import React from 'react'

export const TableCreatorsENS = ({data,handleChgCont}) => {

    const handleClick = (e,index) => {
        window.$creator = data()[index].address
        window.$creatorName = data()[index].accountName
        handleChgCont("callsTable")
    }

    return (
        <>
        <div className='box animate__animated animate__backInLeft animate__slow'>
            <table class={"table"+window.$theme}>
                <thead>
                    <tr>
                        <th scope="col">Account Name</th>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    {data().map( (creator,index) => {
                            return (<tr onClick={(e) => handleClick(e,index)}>
                                <td>{creator.accountName}</td>
                                <td>{creator.address}</td>
                            </tr>)
                        }  
                    )}
                </tbody>
            </table>
        </div>
    </>
    )
}
