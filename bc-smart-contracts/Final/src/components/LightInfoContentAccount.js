
import React from 'react'
import info from '../resources/info.svg'
import './info.css'
export const LightInfoContentAccount = ({title,description,description2}) => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div className="alert alert-info" role="alert">
                
                <h4 className="alert-heading"><img className="info" src={info}></img> {title}</h4>
                <p className="small">{description}</p>
                <p className="small">{description2}</p>
                    <hr/>
                <p className="mb-0 small">Recuerde consultar las FAQS ante cualquier pregunta.</p>
            </div>
        </div>
    )
}
