import React, { useEffect, useState } from 'react'
import { UseContractGeneric } from '../hooks/UseContractGeneric';
import Web3 from 'web3'
import { RegisterNameCard } from './RegisterNameCard';
import {LightInfoContentAccount} from './LightInfoContentAccount'
import { ErrorContentTransaction } from './ErrorContentTransaction';
import { InfoContentTransaction } from './InfoContentTransaction';
import { SuccessContentTransaction } from './SuccessContentTransaction';
export const MyAccount = () => {

    const [metamask, setMetamask] = useState(null)
    const [web3, setWeb3] = useState(null)
    const [networkId,setNetworkId] = useState("")
    const [hasName, setHasName] = useState(false)
    const namehash = require("eth-ens-namehash")
    const [name, setName] = useState(null)
    const [loading, setloading] = useState(true)
    const [infor, setInfor] = useState("")
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)

    useEffect(() => {
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
                
            }
            //Agregar un catch a lo de arriba
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
                setMetamask(true)
            }
        
    }, [])

    const [contractResolver] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "PublicResolverAccounts.json"
    });

    const [contractReverse] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "ReverseRegistrarAccounts.json"
    })

    const handleCheckName = async () => {

        if(metamask){
            if(!!window.$account){
                if(!!contractResolver){
                    let res = await contractResolver.methods.name( namehash.hash((window.$account).substr(2) +".usuarios.cfp.addr.reverse") ).call()
                    if(res.length > 0){
                        setHasName(true)
                        setName(res)
                    }else{
                        setHasName(false)
                    }
                    setloading(false)
                }
            }
        }
    }



     // Eventos que controlan si cambio la red o cambio la cuenta
   
    // Solo se ejecuta si esta metamask conectado
    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
            handleVerify()
        });
    }


    // Obtengo la cuenta ethereum de metamask
    const enableEth = async () => {
        const accounts = await window.ethereum.request({method: "eth_requestAccounts"})
        window.$account = accounts[0]
    }
    
    const handleVerify = async ()=> {
        
        // Si no hay cuenta definida
        if(!window.$account){
            await enableEth()
            await handleCheckName()
        }

        await handleCheckName()
        // Si el contrato se contruyo,es decir si estamos en la red ganache
        
    }

    handleVerify();


    const handleCheckIsAvailable = async (name) =>{

        const apiUrl=`http://localhost:5000/creators`
        
        try{
            const res = await fetch(apiUrl,{
                method: "GET", 
                mode: 'cors'})
            

            const response = await res.json()
            const {creators} = response

            if(creators.length == 0){
                return true
            }else{
                let creatorsName = await Promise.all(creators.map( async (creator) => {
                    if(!!contractResolver){
                        let res = await contractResolver.methods.name( namehash.hash((creator).substr(2) +".usuarios.cfp.addr.reverse") ).call()
                        
                        return res
                    }else{
                        return null
                    }
                }))
            
                return !creatorsName.includes(name)
            }
        }catch(e){
            console.info("No se pudo conectar con la api!")
        }
    }

    const handleRegistry = async (name) => {
        setError(false)
        setInfor("")
        try{
            if(window.$account){
                setInfor("la asignación de un nombre para su cuenta, este es un proceso que se realiza una única vez")
                let res = await contractReverse.methods.setName(name).send({from: window.$account})
                if(res.status === 0){
                    setHasName(false)
                    setError(true)
                    setInfor("")
                    return
                }
                setSuccess(true)
                setInfor("")
                setHasName(true)
            }else{
                setHasName(false)
                setError(true)
            }
            handleVerify();
            await waitFor5seconds();
        } catch(e){
            setHasName(false)
            setInfor("")
            setError(true)
            return
        }
    }

    const waitFor5seconds =  async () => {
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }
        
          sleep(5000)

          setSuccess(false)
    }

    return (
        <>
            { hasName && !loading && <LightInfoContentAccount description={"Nombre de cuenta asociado:  "+name} description2={"Dirección de la cuenta:  "+window.$account} title="Información de la cuenta"/>}
            { !hasName && !loading && <RegisterNameCard handleRegistry={handleRegistry}  handleCheckIsAvailable={handleCheckIsAvailable}/>}
            {error && <ErrorContentTransaction/>}
            {infor.length > 0 && <InfoContentTransaction description={infor} /> }
            {success && <SuccessContentTransaction description=" Su cuenta a sido registrada y se encuentra habilitada!"/>}
        </>
    )
}
