import React, { useEffect, useState } from 'react'
import { useGetCall } from '../hooks/UseGetCall';
import { TableCalls } from './TableCalls';
import { TableCallsENS } from './TableCallsENS';
import './ListContent.css'
import { useGetCallNames } from '../hooks/UseGetCallNames';
import { UseContractGeneric } from '../hooks/UseContractGeneric';
import Web3 from 'web3'
export const ListCalls = ({handleChgCont}) => {
   
    const [creator] = useState(window.$creator);
    const [response,data] = useGetCall();
    const [metamask, setMetamask] = useState(null)
    const [web3, setWeb3] = useState(null)
    const [networkId,setNetworkId] = useState("")
    const namehash = require("eth-ens-namehash")


    useEffect(() => {
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
                
            }
            
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
                setMetamask(true)
            }
        
    }, [])


    const [contractResolver] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "PublicResolverCalls.json"
    });

    const [ responseNames, dataAccountNames ] = useGetCallNames(data,contractResolver);

    const getList = () => {
        const array = []
        if(metamask){
            for(let i = 0; i<data.length; i++){
                array.push({address: data[i], callName: dataAccountNames[i]})
            }
        }
        return array
    }

    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
        });
    }
    
    return (
        <>
            {!response.loading &&<h2 className={window.$theme}>List of calls from creator: {!!contractResolver? window.$creatorName:creator}</h2>}
            {!response.loading && contractResolver==null && <TableCalls handleChgCont={handleChgCont} data={data}/>}
            {!response.loading && !!contractResolver && metamask && !responseNames && <TableCallsENS data={getList} accountNames={dataAccountNames} handleChgCont={handleChgCont}/>} 
        </>
    )
}
