
import React from 'react'
import info from '../resources/info.svg'
import './info.css'
export const InfoContentTransaction = ({description}) => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div className="alert alert-primary " role="alert">
                
                <h4 className="alert-heading"><img className="info" src={info}></img> Transacción pendiente</h4>
                <p>La transacción que esta pendiente se corresponde con {description}</p>
                    <hr/>
                <p className="mb-0">Recuerde consultar las FAQS ante cualquier pregunta.</p>
            </div>
        </div>
    )
}
