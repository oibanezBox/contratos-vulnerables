import React, { useEffect, useState } from 'react'
import { UseContract } from '../hooks/UseContract';
import { UseContractGeneric } from '../hooks/UseContractGeneric';
import Web3 from 'web3'
import { CallCreatrionContent } from './CallCreationContent';
import { InfoContentTransaction } from './InfoContentTransaction';
import { ErrorContentTransaction } from './ErrorContentTransaction';
import { InfoContent } from './InfoContent';
import { SuccessContentTransaction } from './SuccessContentTransaction';
import { LightInfoContentAccount } from './LightInfoContentAccount';

export const RegisterNewCall = () => {

    const [metamask, setMetamask] = useState(null)
    const [networkId,setNetworkId] = useState("")
    const [isRegistered, setIsRegistered] = useState(null)
    const [isAutorized,setIsAutorized] = useState(null)
    const [creation, setCreation] = useState(false)
    const [hasName, setHasName] = useState(null)
    const namehash = require("eth-ens-namehash")
    const [loading, setloading] = useState(true)
    const [infor, setInfor] = useState("")
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)

    // Si el netid es el de ganache entonces se construye el contrato
    

    useEffect(() => {
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
                
            }
            //Agregar un catch a lo de arriba
                getNetId()
                setMetamask(true)
            }
        
    }, [])

   
    const keccak256 = Web3.utils.keccak256;
    
    
    const [contract] = UseContract({
        "networkId": networkId,
    });

    // Resolverdor para el dominio de usuarios.cfp
    const [contractResolver] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "PublicResolverAccounts.json"
    });

    // Resolutor reverso para los dominio llamados.cfp
    const [contractReverseCalls] = UseContractGeneric({
        "networkId": networkId,
        "contractName": "ReverseRegistrarCalls.json"
    });

    
    const handleCheckName = async () => {

        if(metamask){
            if(!!window.$account){
                if(!!contractResolver){
                    let res = await contractResolver.methods.name( namehash.hash((window.$account).substr(2) +".usuarios.cfp.addr.reverse") ).call()
                    if(res.length > 0){
                        setHasName(true)
                    }else{
                        setHasName(false)
                    }
                    setloading(false)
                }
            }
        }
    }


    // Eventos que controlan si cambio la red o cambio la cuenta
   
    // Solo se ejecuta si esta metamask conectado
    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
            handleVerify()
        });
    }


    // Obtengo la cuenta ethereum de metamask
    const enableEth = async () => {
        const accounts = await window.ethereum.request({method: "eth_requestAccounts"})
        window.$account = accounts[0]
    }
    
    const handleVerify = async ()=> {
        
        // Si no hay cuenta definida
        if(!window.$account){
            await enableEth()
            await handleCheckName()
        }

        await handleCheckName()
        // Si el contrato se contruyo,es decir si estamos en la red ganache
        if(!!contract){
            // Me fijo si se registro previamente
            let res = await contract.methods.isRegistered(window.$account).call()
            setIsRegistered(res)

            // Me fijo si ya se autorizo
            res = await contract.methods.isAuthorized(window.$account).call()
            setIsAutorized(res)
        }
    }

    handleVerify();


    const handleRegister = async (e) => {
        setInfor("")
        setSuccess(false)
        setError(false)
        e.preventDefault();
        setInfor("La autorización de su cuenta para crear llamados")
        let res = await contract.methods.register().send({from: window.$account})
        if(res.status === 0){
            setInfor("")
            setError(true)
            setCreation(false)
            return
        }
        handleVerify()
    }


    const handleCreationPanel = (e) =>{
        setInfor("")
        setSuccess(false)
        setError(false)
        setCreation(true)
    }


    function toTimestamp(strDate){
        var datum = Date.parse(strDate);
        return datum/1000;
    }
       

    const handleCreate = async (e,name,callid,date,desc) => {
       try{
        
        if(!!contract && callid && date){ 
            setInfor("el registro de un llamado")
            let res = await contract.methods.create(callid,toTimestamp(date)).send({from: window.$account})
            console.log(res)
            if(res.status === 0){
                setCreation(false)
                setError(true)
                setInfor("")
                return
            }
            setInfor("la asignación de un nombre para el llamado")
            setError(false)
            res = await contractReverseCalls.methods.setName(name,keccak256(callid.substr(2))).send({from: window.$account})
            if(res.status === 0){
                setInfor("")
                setError(true)
                setCreation(false)
                return
            }

            if(desc.length > 0){
                setInfor("la asignación de una descripción para el llamado")
                setError(false)
                res = await contractReverseCalls.methods.setText(keccak256(callid.substr(2)),"desc",desc).send({from: window.$account})
                if(res.status === 0){
                    setInfor("")
                    setCreation(false)
                    setError(true)
                    return
                }
            }
            setSuccess(true)
            setInfor("")
            setCreation(false)
            
            }else{
                setCreation(false)
                setError(true)
            }
        }catch(e){
            setInfor("")
            setCreation(false)
            setError(true)
            return
        }
        
    }

    
    return (
        <div className="center">
            {hasName && !!hasName && isAutorized===false && isRegistered === false && metamask===true && <button className="btn-success mt-4" onClick={ (e) => handleRegister(e)}> Regsitrame!</button>}
            {!hasName && !loading  && <InfoContent title="Nombre de cuenta no establecido" description="Usted no tiene asociado ningun nombre de cuenta! Por favor registre uno en la sección my account." />}
            {isAutorized===true && !creation && <LightInfoContentAccount 
            title="Crear un llamado" 
            description="En esta sección puede crear llamados y permitir que los usuarios presenten sus propuestas" 
            description2="Nota: este proceso implica cargos monetarios adicionales"/>}
            {isAutorized===true && !loading && !creation && <button className="btn-primary mt-4 animate__animated animate__fadeIn animate__slow" onClick={(e) => handleCreationPanel(e)}>Crear llamado!</button>}
            { !loading && creation && <CallCreatrionContent handleCreate={handleCreate}/>}
            {error && <ErrorContentTransaction/>}
            {infor.length > 0 && <InfoContentTransaction description={infor} /> }
            {success && <SuccessContentTransaction description=" Su llamado a sido registrado y se encuentra habilitado!"/>}
            {isAutorized===false && isRegistered===true && <InfoContent title="Solicitud a la espera" description="Su solicitud esta siendo procesada, este proceso puede demorar entre 24 y 48 hs." />}
        </div>
    )
}
