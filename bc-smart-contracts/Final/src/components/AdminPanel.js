import React, { useState } from 'react'
import { UseGetPendingRegistrations } from '../hooks/UseGetPendingRegistrations'
import { ErrorContentTransaction } from './ErrorContentTransaction'
import { LightInfoContentAccount } from './LightInfoContentAccount'
import { SuccessContentTransaction } from './SuccessContentTransaction'
import { TablePendingRegistrations } from './TablePendingRegistrations'

export const AdminPanel = () => {

    const [pageCount, setPageCount] = useState(0)
    const [ response,data,refresh ] = UseGetPendingRegistrations(pageCount)
    const [list, setList] = useState([])
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)
    const apiUrl=`http://localhost:5000/authorize`

    const waitFor5seconds =  async (e) => {
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }
        
          await sleep(5000)
          setError(false)
          setSuccess(false)
          refresh()
    }

    const handleCheck = (e) => {
        if(e)

        if(e.target.checked){
            if(!list.includes(e.target.value)){
                setList([...list,e.target.value])
            }
        }else{
            if(list.includes(e.target.value)){
                setList([...list.filter( valor => valor !== e.target.value)])
            }
        }
    }

    const handleAutorize = async (e) => {
        try{
            for(let i=0; i < list.length; i++){
                const object = {
                    "sender": window.$account,
                    "address": list[i]
                }
               const response = await fetch(apiUrl,{
                    method: "POST", 
                    mode: 'cors',
                    headers: {
                        'Content-Type': 'application/json'
                      },
                    body: JSON.stringify(object)
               })
                setSuccess(true)
                setList([])
                waitFor5seconds(e)
                refresh()
                
               if(!response.ok){
                throw new Error('Error al tratar de autorizar la cuenta: '+list[i]);
               }
            }
        }catch(error){
            console.log(error)
            //window.alert(e.message)
            setError(true)
            waitFor5seconds(e)
            return
        }
        refresh()
    }

  

    console.log(list.length)
    return (
        <div>
            {response.loading?<h2 className={window.$theme}>Loading please wait!</h2>:<h2 className={window.$theme}>List of pending registrations: </h2>}
            {!response.loading && !!data && data.data?.length !== 0 &&<TablePendingRegistrations data={data.data} handleCheck={handleCheck}/>}
            {!response.loading && data.data?.length === 0 && <LightInfoContentAccount title="No tiene autorizaciones pendientes" description="Por el momento no hay trabajo que hacer! 😃"/>}
            {list.length !== 0 && <button className="btn-primary mt-4 animate__animated animate__fadeIn animate__slow" onClick={ (e) => handleAutorize(e)}>Autorizar!</button>}
            {error && <ErrorContentTransaction/> }
            {success && <SuccessContentTransaction description="La/as cuentas seleccionadas fueron autorizadas!"/>}
        </div>
    )
}
