import React from 'react';

import './Login.css'
import avatar from '../resources/avatar.svg'

export default function Login(props){


    return (
        <>
            <div className={"wrapper fadeInDown"}>
                <div id={"formContent-"+props.theme}>
                    <div className="fadeIn first">
                        <img src={avatar} id="icon" alt="User Icon" />
                    </div>

                  
                    <form>
                        <input type="text" id="login" className="fadeIn second" name="login" placeholder="Usuario"/>
                        <input type="password" id="password" className="fadeIn third" name="login" placeholder="Contraseña"/>
                        <input type="submit" className="fadeIn fourth" value="Log In"/>
                    </form>

                    <div id={"formFooter-"+props.theme}>
                        <p className="underlineHover cu" >Olvidaste tu contraseña?</p>
                    </div>

                </div>
                </div>
        </>
    )
}