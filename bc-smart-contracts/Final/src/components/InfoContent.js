
import React from 'react'
import info from '../resources/info.svg'
import './info.css'
export const InfoContent = ({title,description}) => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div className="alert alert-warning " role="alert">
                
                <h4 className="alert-heading"><img className="info" src={info}></img> {title}</h4>
                <p className="small">{description}</p>
                    <hr/>
                <p className="mb-0 small">Recuerde consultar las FAQS ante cualquier pregunta.</p>
            </div>
        </div>
    )
}
