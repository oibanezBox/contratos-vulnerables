import React, {useEffect, useState} from 'react';
import { UseContract } from '../hooks/UseContract';
import tsaLogo from '../resources/tsa.png'
import './NavBar.css'
import Web3 from 'web3'

export default function NavBar(props){
    const [theme, setTheme] = useState("light");
    
    const [metamask, setMetamask] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);
    const [web3, setWeb3] = useState(null)
    const [networkId,setNetworkId] = useState("")


    // Obtengo la cuenta ethereum de metamask
    const enableEth = async () => {
        const accounts = await window.ethereum.request({method: "eth_requestAccounts"})
        window.$account = accounts[0]
    }

    useEffect(() => {
        if(!!window.ethereum){
            setMetamask(true);
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
                
            }
            //Agregar un catch a lo de arriba
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
                enableEth()
            }  
    }, [])

    // Construyo el contrato si la red es en la que esta desplegado
    const [contract] = UseContract({
        "networkId": networkId,
    });

    // Verifico si la cuenta es la misma que creo la factoria (es dueña)
    const handleVerifyAdmin = async () =>{
        let owner = await contract.methods.owner().call()
        if(web3.utils.toChecksumAddress(window.$account) === owner){
            setIsAdmin(true)
        }else{
            setIsAdmin(false)
            props.handleChgCont("creatorsTable")
        }
    }


    useEffect(() => {
        const promiseAdmin = async () => {
            if(!!contract){
                handleVerifyAdmin()   
            }
        }
        promiseAdmin()
    }, [contract])
    


    // Eventos que controlan si cambio la red o cambio la cuenta
   
    // Solo se ejecuta si esta metamask conectado
    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetworkId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
            if(!!contract){
                handleVerifyAdmin()   
                
            }
        });
    }

    
    return(
        <>
            <nav className={"navbar navbar-expand-lg navbar-"+theme+" bg-"+theme}>
                <div className="container-fluid">
                    {/* <a className="navbar-brand" href="#">Box Custodia</a> */}
                    <img onClick={ (e) => props.login(false)} className="ico" alt="" src={tsaLogo}></img>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {/* <li className="nav-item">
                        <a  className="nav-link active" aria-current="page" href="#">TSA</a>
                        </li> */}

                        <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Menu
                        </a>
                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a onClick={(e) => props.handleChgCont("creatorsTable")}className="dropdown-item" href="#">Home</a></li>
                            <li><hr className="dropdown-divider"/></li>
                            <li>{metamask?<a className="dropdown-item" onClick={ (e) => props.handleChgCont("account")} href="#" tabIndex="-1" aria-disabled="true">My calls</a>:<a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">My calls</a>}</li>
                            <li>{metamask?<a className="dropdown-item" onClick={ (e) => props.handleChgCont("myaccount")} href="#" tabIndex="-1" aria-disabled="true">My account</a>:<a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">My account</a>}</li>
                            <li><a className="dropdown-item" href="#">FAQS</a></li>
                        </ul>
                        </li>
                        <li className="nav-item">
                        {isAdmin?<a className="nav-link" onClick={ (e) => props.handleChgCont("adminPanel")} href="#" tabIndex="-1" aria-disabled="true">Administrar!</a>:<a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Administrar</a>}
                        </li>
                    </ul>
                    <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                        <button disabled={true} className="btn btn-outline-success" type="submit">Search</button>
                        <button className={"btn btn-outline btn-theme-"+theme} onClick={
                            (e) => {
                                e.preventDefault();    
                                theme === "dark"? setTheme("light") : setTheme("dark");
                                props.onChangeTheme(theme);
                                window.$theme = theme
                            }
                        }>Theme</button>
                        {/* <ThemeButton theme={theme} onClick={ (e) => {e.preventDefault();
                            theme === "light"? setTheme("dark") : setTheme("light");}}>Theme</ThemeButton> */}
                    </form>
                    </div>
                </div>
            </nav>
        </>
    )
}