import React from 'react'

export const TableCallsENS = ({data,handleChgCont}) => {

    const handleClick = (e,index) => {
        window.$callid = data()[index].address
        window.$callidName = data()[index].callName
        handleChgCont("cfp")
    }

    return (
        <>
        <div className='box animate__animated animate__backInLeft animate__slow'>
            <table class={"table"+window.$theme}>
                <thead>
                    <tr>
                        <th scope="col">Call Name</th>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    {data().map( (creator,index) => {
                            return (<tr onClick={(e) => handleClick(e,index)}>
                                <td>{creator.callName}</td>
                                <td>{creator.address}</td>
                            </tr>)
                        }  
                    )}
                </tbody>
            </table>
        </div>
    </>
    )
}
