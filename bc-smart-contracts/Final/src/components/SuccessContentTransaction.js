
import React from 'react'
import success from '../resources/check.svg'
import './info.css'
export const SuccessContentTransaction = ({description}) => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div className="alert alert-success " role="alert">
                
                <h4 className="alert-heading"><img alt="" className="info" src={success}></img> Transacción exitosa!</h4>
                <p>La/s transacción/es se realizaron de forma exitosa. {description} </p>
                    <hr/>
                <p className="mb-0 mini">Recuerde que puede revisar las FAQS ante cualquier consulta!.</p>
            </div>
        </div>
    )
}
