
import React from 'react'
import './ProposalContent.css'

export const ProposalContent = ({data}) => {
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div class={"card"+window.$theme}>
                <div class="card-body">
                    <h4 class="card-title">Propuesta registrada</h4>
                    <p class="card-text mt-2 cup">BlockNumber   {data.blockNumber}</p>
                    <p class="card-text cup">Sender   {data.sender}</p>
                    <p class="card-text cup">Timestamp   {data.timestamp}</p>
                </div>
            </div>
        </div>
    )
}
