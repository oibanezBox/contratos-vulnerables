
import React, { useState } from 'react'
import './ProposalContent.css'
import sha256 from 'js-sha256'

export const CallCreatrionContent = ({handleCreate}) => {

    const [date, setDate] = useState(new Date())
    const [ok, setOk] = useState(null)
    const [callid, setCallid] = useState(null)
    const [okCallid, setOkCallid] = useState(true)
    const [text, setText] = useState("")
    const [desc, setDesc] = useState("")
    
    const handleChangeDate = (e) => {
        setDate(e.target.value)
        if(Date.parse(e.target.value) <= Date.parse(new Date())){
            setOk(false)
        }else{
            setOk(true)
        }
    }

    const handleChangeCallid = async (e) => {
        const hs =sha256.create()
        hs.update(e.target.value+window.$account)
        setCallid(`0x${hs.hex()}`)
        setText(e.target.value)
        
        let res = await fetch(`http://localhost:5000/calls/isRegister/0x${hs.hex()}` ,{
            method: "GET",
            mode: "cors"
        })
        let json= await res.json()
        const {isRegistered} = json

    

         if(isRegistered ){
             setOkCallid(false)
         }else{
             setOkCallid(true)
         }
    }

    const handleChangeDesc = (e) => {
        setDesc(e.target.value)
    }

    const handleCreation = (e) =>{
        setOk(null)
        setOkCallid(true)
        if(callid === null || text === ""){
            const hs =sha256.create()
            hs.update(String(Date.now))
            handleCreate(e,`0x${hs.hex()}`,date)
        }else{
            handleCreate(e,text,callid,date,desc)
        }
        
    }
    console.log(text.length)
    return (
        <div className="box animate__animated animate__backInLeft animate__slow">
            <div class={"card"+window.$theme}>
                <div class="card-body">
                    <h4 class="card-title">Complete los siguientes campos:</h4>
                    <p class="card-text mt-2 cup">Fecha de cierre: <input type="date" onChange={(e) => handleChangeDate(e)} value={date}></input></p>
                    <p class="card-text mt-2 cup">Call id*: <input type="text" onChange={(e) => handleChangeCallid(e)} value={text}></input></p>
                    <p class="card-text mt-2 cup">Descripción:      <input type="text" onChange={(e) => handleChangeDesc(e)} value={desc}></input></p>
                    { !!callid && text.length > 0 && <p class="card-text mt-2 mini">{callid}</p>}
                    { ok === false && <p class="card-text mt-2 cup is-warning"> La fecha de cierre debe tener al menos 1 día de diferencia con respecto a la fecha actual!</p>}
                    { okCallid === false  && <p class="card-text mt-2 cup is-warning"> Callid ingresado no esta disponible ingrese otro!</p> }
                    { !!ok && !!okCallid &&
                     <button 
                     disabled={!ok || text.length < 3}
                     hidden={!ok ? true : false}
                     className="btn btn-outline-danger customBtn animate__animated animate__backInDown animate__slow"
                     onClick={handleCreation}
                     >Crear llamado!</button>   
                    }
                </div>
            </div>
        </div>
    )
}