import React, { useEffect, useState } from 'react'
import sha256 from 'js-sha256'
import Web3 from 'web3'
import { UseContract } from '../hooks/UseContract'
import { UseContractCFP } from '../hooks/UseContractCFP'
import svg from '../resources/fox.svg'
import cont from '../resources/contract.svg'
import { ProposalContent } from './ProposalContent'
import './CFP.css'
import { UseContractGeneric } from '../hooks/UseContractGeneric'
import { InfoContentTransaction } from './InfoContentTransaction'
import { SuccessContentTransaction } from './SuccessContentTransaction'
import { ErrorContentTransaction } from './ErrorContentTransaction'
import { InfoContent } from './InfoContent'
import { LightInfoContentAccount } from './LightInfoContentAccount'
export const CFP = () => {
    
    const [hash,setHash] = useState({
        "file":"",
        "name":"",
        "hash": "",
        "loading": true
    })
    const [web3, setWeb3] = useState(null)
    const [netId,setNetId] = useState("")
    const [proposal, setProposal] = useState(null)
    const [metamask, setMetamask] = useState(null)
    const namehash = require("eth-ens-namehash")
    const [infor, setInfor] = useState("")
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)
    const [warning, setWarning] = useState("")
    const [state, setState] = useState(false)
    const [description,setDescription] = useState("")
    useEffect(() => {
        //window.$theme = 'dark'
        
        if(!!window.ethereum){
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetId(net)
            }
            setWeb3(
                new Web3(window.ethereum)
                )
                getNetId()
                setMetamask(true)
            }
        }, [])
        
        
        const [contract] = UseContract({
            "networkId": netId,
        })

        const [contractCFP] = UseContractCFP({
        "networkAdd": window.$callid
    })
        
    const handleChgFile = async (e)=>{
            if(!!e.target.files[0]){
            setHash({
            "file":e.target.files[0],
            "name":e.target.files[0].name,
            "hash": ""
            })   
            let reader = new FileReader();
            
            reader.readAsArrayBuffer(e.target.files[0])
            reader.onload = function(e) {
                
                const arrayBuffer = new Uint8Array(reader.result);
                const hs =sha256.create()
                hs.update(arrayBuffer)
                setHash({
                    ...hash,
                    "loading": false,
                    "hash": `0x${hs.hex()}`
                })
                setProposal(null)
            }   
        }
    }

    const [contractResolver] = UseContractGeneric({
        "networkId": netId,
        "contractName": "PublicResolverAccounts.json"
    });

    const [contractResolverCalls] = UseContractGeneric({
        "networkId": netId,
        "contractName": "PublicResolverCalls.json"
    });

    const handleCheckDesc = async () =>{
        if(metamask && !!contractResolver){
            try{
                let res = await contractResolverCalls.methods.text( namehash.hash((window.$callid).substr(2) +".llamados.cfp.addr.reverse"),"desc" ).call()
                
                if(res.length > 0){
                    setDescription(res)
                }else{
                    setDescription("")
                }
            }catch(error){
                setDescription("")
            }
        }
    }

    handleCheckDesc()

    const cleanNotification = ()=>{
        setProposal(null)
        setInfor("")
        setError(false)
        setSuccess(false)
        setWarning("")
        setState(false)
    }

    const handleRegister = async (e) =>{
        // Limpio notificaciones
        cleanNotification()
        // ---------------
        e.preventDefault()
        try{
            let apiurl = `http://localhost:5000/proposal-data/${window.$callid}/${hash.hash}`
            const response = await fetch(apiurl,{
                method: "GET", 
                mode: 'cors'
            })

            let data = await response.json()
            if(!data.message){
                setWarning(data.blockNumber)
                return
            }

            // if(res.blockNumber !== "0"){
            //     setWarning(res.blockNumber)
            //     return
            // }
            apiurl = "http://localhost:5000/register-proposal"
            let res = await fetch(apiurl, {
                method: "POST", 
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({callId: window.$callid, proposal: hash.hash})
            })
            const json = await res.json()
            if(json.message == "OK"){
                setSuccess(true)
                waitFor5seconds(e)
            }else{
                setWarning("")
                setInfor("")
                setError(true)
                return
            }
        }catch(e){
            console.log(e)
            setInfor("")
            setError(true)
            return
        }
      
    }



    const handleVerify = async (e) => {
        // Limpio notificaciones
        cleanNotification()
        // ---------------
        e.preventDefault()

        const apiurl = `http://localhost:5000/proposal-data/${window.$callid}/${hash.hash}`
        const response = await fetch(apiurl,{
            method: "GET", 
            mode: 'cors'
        })

        const data = await response.json()
        if(data.message){
            setProposal(null)
            setState(true)
        }else{
            if(metamask && !!contractResolver){
                let res = await contractResolver.methods.name( namehash.hash((data.sender).substr(2) +".usuarios.cfp.addr.reverse") ).call()
                if(res.length > 0){
                    data.sender = res
                }
                setProposal(data)
            }else{
                setProposal(data)
            }
        }
        
    }


    const enableEth = async () => {
        const accounts = await window.ethereum.request({method: "eth_requestAccounts"})
        window.$account = accounts[0]
    }

    const handleMetaMask = async (e) => {
        // Limpio notificaciones
        cleanNotification()
        // ---------------
        e.preventDefault()
        if(!window.$account){
            await enableEth()
        }
        try{
            if(!window.$account) return
            let res = await contractCFP.methods.proposalData(hash.hash).call()
            if(res.blockNumber !== "0"){
                setWarning(res.blockNumber)
                return
            }else{
                if(window.$account && window.$callid && hash.hash){
                    res = await contract.methods.registerProposal(window.$callid,hash.hash).send({from: window.$account})
                    if(res.status !== 0){
                        setSuccess(true)
                        waitFor5seconds(e)
                    }else{
                        setWarning("")
                        setInfor("")
                        setError(true)
                        return
                    }
                }else{
                    setWarning("")
                    setInfor("")
                    setError(true)
                    return
                }
            }
        }catch(e){
            setInfor("")
            setError(true)
            return
        }
    }

    if(window.ethereum !== undefined){

        window.ethereum.on('chainChanged',async (networkVersion) => {
            const getNetId = async () =>{
                const net = await window.ethereum.request({method: "net_version"})
                setNetId(net)
            }
            getNetId()
        });

        window.ethereum.on('accountsChanged', (accounts) =>  {
            window.$account = accounts[0]
        });
    }



    const waitFor5seconds =  async (e) => {
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }
        
          await sleep(5000)

          setSuccess(false)
          handleVerify(e)
    }

    return (
        <>
            <div className={"box"+window.$theme}>
                {!!web3 && <img className="animate__animated animate__fadeInDown animate__slow" src={svg} alt=""></img>}
                {!!contract && <img className="animate__animated animate__fadeInDown animate__slow contract" src={cont} alt=""></img>}
                <form>
                    <label for="formFileLg" class="form-label animate__animated animate__fadeInDown animate__slow">Selecione el archivo que contine la propuesta!</label>
                    <div className="archivo">
                    <input onChange={handleChgFile} className="form-control archivoInput animate__animated animate__fadeInDown animate__slow" id="formFileLg" type="file"/>
                    </div>
                    {!hash.loading && <p>Hash: {hash.hash}</p>}
                    <button
                    disabled={hash.loading}
                    className="btn  btn-outline-success customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleRegister}
                    >Registrar Propuesta </button>   
                    <button
                    disabled={hash.loading}
                    className="btn btn-outline-info customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleVerify}
                    >Verificar Propuesta </button>   
                    <button 
                    disabled={hash.loading}
                    hidden={!contract? true : false}
                    className="btn btn-outline-danger customBtn animate__animated animate__backInDown animate__slow"
                    onClick={handleMetaMask}
                    >Sellar con mi cuenta </button>   
                </form>
            </div>
                {description.length !== 0 && <LightInfoContentAccount 
                title="Breve descripción"
                description={description}/>}
                {state && <LightInfoContentAccount
                title="Propuesta no registrada"
                description="La propuesta no fue registrada aún"
                description2= "Puede registrala con su cuenta o de forma anónima"  />}
                {warning && <InfoContent 
                title="Propuesta previamente registrada" 
                description={"La propuesta que intenta presentar, se registro previamente en el bloque Nro: "+warning} />}
                {error && <ErrorContentTransaction/>}
                {infor.length > 0 && <InfoContentTransaction description={infor} /> }
                {success && <SuccessContentTransaction description=" Su propuesta a sido registrada con éxito!"/>}
                {!!proposal && <ProposalContent data={proposal}></ProposalContent>}  
        </>
    )
}
