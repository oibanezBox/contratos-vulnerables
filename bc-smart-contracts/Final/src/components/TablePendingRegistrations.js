import React from 'react'




export const TablePendingRegistrations = ({data,handleCheck}) => {
    return (
        data !== undefined?(<>
          <div className='box animate__animated animate__backInLeft animate__slow'>
            <table class={"table"+window.$theme}>
                <thead>
                    <tr>
                        <th scope="col">Selected</th>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map( (creator,index) => {
                            return (<tr>
                                <td><input type="checkbox" value={creator} onChange={ (e) => handleCheck(e)}></input></td>
                                <td>{creator}</td>
                            </tr>)
                        }  
                    )}
                </tbody>
            </table>
        </div>   
        </>): (<></>)
    )
}
