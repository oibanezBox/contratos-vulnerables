import { useEffect, useState } from 'react'
import Web3 from 'web3'

export const UseContractGeneric = ({networkId,contractName}) => {

    const [contract, setContract] = useState(null);
    
    useEffect(() => {
        const web3 = window.ethereum? new Web3(window.ethereum):undefined
        if(web3){
            const data = require(`../ethereum/build/contracts/${contractName}`)
            
           const address = data.networks?.[networkId]?.address
           
           if(address){
               setContract(new web3.eth.Contract(data.abi,address))
           }else{
               setContract(null)
           }

        }
       
    }, [networkId,contractName])

    return [contract,setContract]
}
