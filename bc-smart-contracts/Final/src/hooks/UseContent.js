import { useState } from "react"


export const UseContent = () => {
    
    const [content, setContent] = useState(
        {
            "creatorsTable": true,
            "callsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false,
            "account": false
        }
    );

    const handleChangeContent = ( contentToChg ) => {
        
        if( contentToChg === "callsTable") setContent({
            "callsTable": true,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false,
            "account": false,
            "adminPanel": false,
            "myaccount": false
        })

        if( contentToChg === "creatorsTable") setContent({
            "creatorsTable": true,
            "callsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": false,
            "account": false,
            "adminPanel": false,
            "myaccount": false
        })

        if( contentToChg === "cfp") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": true,
            "cfpRegister": false,
            "cfpVerify": false,
            "account": false,
            "adminPanel": false,
            "myaccount": false
        })
        if( contentToChg === "cfpRegister") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": true,
            "cfpVerify": false,
            "account": false,
            "adminPanel": false,
            "myaccount": false
        })
        if( contentToChg === "cfpVerify") setContent({
            "callsTable": true,
            "creatorsTable": false,
            "cfp": false,
            "cfpRegister": false,
            "cfpVerify": true,
            "account": false,
            "adminPanel": false,
            "myaccount": false
        })

        if( contentToChg === "account") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": false,
            "account": true,
            "cfpRegister": false,
            "cfpVerify": false,
            "adminPanel": false,
            "myaccount": false
        })

        if( contentToChg === "myaccount") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": false,
            "account": false,
            "cfpRegister": false,
            "cfpVerify": false,
            "adminPanel": false,
            "myaccount": true
        })

        if( contentToChg === "adminPanel") setContent({
            "callsTable": false,
            "creatorsTable": false,
            "cfp": false,
            "account": false,
            "cfpRegister": false,
            "cfpVerify": false,
            "adminPanel": true,
            "myaccount": false
        })
    
    }


    return [content, handleChangeContent]
    
}
