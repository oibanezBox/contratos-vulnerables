import { useEffect, useState } from 'react'


export const useGetCallNames = (data,contractResolver) => {
    const [response, setResponse] = useState({
        "loading": true
    });
    const [metamask, setMetamask] = useState(null)
    const namehash = require("eth-ens-namehash")
    const [names, setNames] = useState([]);
    

    useEffect(() => {
        
        if(!!window.ethereum){
            setMetamask(true)
        }
        const getResponse = async ()=>{
            if(metamask){

                if(!!contractResolver){
                    let namesGet =  await Promise.all(data.map( async (callid) => {
                        let name = await contractResolver.methods.name( namehash.hash((callid).substr(2) +".llamados.cfp.addr.reverse") ).call()
                        return name
                    }))
                    setNames(namesGet)
                }
            }
            setResponse(false)
        }

        getResponse()
    }, [data,contractResolver,metamask,namehash])

    return [response,names]
}
