import  React,{ useEffect, useState } from 'react'

export const UseGetPendingRegistrations = (page = 0) => {
    const [response, setResponse] = useState({
        "loading": true
    });
 
    const [data, setData] = useState([]);


    const getResponse = React.useCallback(async ()=>{
        const apiUrl=`http://localhost:5000/pendingRegister/${window.$account}/${page}`
    
        try{
    
            const res = await fetch(apiUrl,{
                method: "GET", 
                mode: 'cors',
            })
            
            const response = await res.json()
            const {message} = response
                
            if(message){
                setResponse({
                    "err":false,
                    "message": message,
                    "loading": false
                })
                
            }else{
                setResponse({
                    ...response,
                    "message": "ok",
                    "loading": false
                })
                setData({data: response.pendingAccounts,
                        pages: response.pages,
                        pageSize: response.pageSize})
                }
            }catch(err){
                console.log(err)
                }
            
            },[page])

    useEffect(() => {
            getResponse()   
            }
        , [page,getResponse])

    const refresh = () =>{
        getResponse()
    } 
        

    return [response,data,refresh]
}
