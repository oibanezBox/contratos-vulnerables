const ENSRegistry = artifacts.require("./ENSRegistry.sol");
const FIFSRegistrar = artifacts.require("./FIFSRegistrar.sol");
const ReverseRegistrar = artifacts.require("./ReverseRegistrar.sol");
const PublicResolver = artifacts.require("./PublicResolver.sol");

const PublicResolverAccounts = artifacts.require("./PublicResolverAccounts.sol");
const ReverseRegistrarAccounts = artifacts.require("./ReverseRegistrarAccounts.sol");
const PublicResolverCalls = artifacts.require("./PublicResolverCalls.sol");
const ReverseRegistrarCalls = artifacts.require("./ReverseRegistrarCalls.sol");

const CFPFactory = artifacts.require("CFPFactory");
const web3 = new (require("web3"))();
const namehash = require("eth-ens-namehash");
const keccak256 = web3.utils.keccak256;


module.exports = async function(deployer, network, accounts) {
	deployer.deploy(CFPFactory);
	var tld = "cfp";
	// Primero desplegamos el registro (registry)
	await deployer.deploy(ENSRegistry);
	let ens = await ENSRegistry.at(ENSRegistry.address);
	// Conviene desplegar un resolutor público
	await deployer.deploy(PublicResolver,ENSRegistry.address);
	let publicResolver = await PublicResolver.at(PublicResolver.address);
	// Para facilitar su uso, le damos un nombre
	await ens.setSubnodeOwner("0x0", keccak256("resolver"), accounts[0]);
	// Declaramos el resolver de este nuevo dominio de primer nivel
	await ens.setResolver(namehash.hash("resolver"), PublicResolver.address);
	// Registramos en el resolver su propia dirección asociada al nuevo dominio
	await publicResolver.setAddr(namehash.hash("resolver"), PublicResolver.address);
	// Desplegamos un registrador para el dominio `tld`
	await deployer.deploy(FIFSRegistrar, ENSRegistry.address, namehash.hash(tld));
	let registrar = await FIFSRegistrar.at(FIFSRegistrar.address);
	// Transferimos la propiedad del dominio `tld` al nuevo registrador
	await ens.setSubnodeOwner("0x0", keccak256(tld), FIFSRegistrar.address);
	// Desplegamos el registrador reverso
	await deployer.deploy(ReverseRegistrar, ENSRegistry.address, PublicResolver.address);
	// Transferimos la propiedad del dominio "addr.reverse" al registrador
	await ens.setSubnodeOwner("0x0", keccak256("reverse"), accounts[0]);
	await ens.setSubnodeOwner(namehash.hash("reverse"), keccak256("addr"), accounts[0]);
	
	
	
	// Despliego un resolver distinto para las cuentas:
	await deployer.deploy(PublicResolverCalls,ENSRegistry.address);
	let publicResolverCalls = await PublicResolverCalls.at(PublicResolverCalls.address);
	
	//Transferimos la propiedad del dominio llamados al registrador
	await registrar.register(keccak256("llamados"),accounts[0]);
	// Declaramos un resolver para este nuevo dominio llamados;
	await ens.setResolver(namehash.hash("llamados.cfp"), PublicResolverCalls.address);
	
	// Para facilitar su uso, le damos un nombre
	await ens.setSubnodeOwner(namehash.hash("llamados.cfp"), keccak256("resolver"), accounts[0]);
	// Declaramos el resolver de este nuevo dominio de primer nivel
	await ens.setResolver(namehash.hash("resolver.llamados.cfp"), PublicResolverCalls.address);
	// Registramos en el resolver su propia dirección asociada al nuevo dominio
	await publicResolverCalls.setAddr(namehash.hash("resolver.llamados.cfp"), PublicResolverCalls.address);
	
	// Configurando el resolutor inverso para llamados.cfp
	// Desplegamos el registrador reverso
	await deployer.deploy(ReverseRegistrarCalls, ENSRegistry.address, PublicResolverCalls.address);
	// Transferimos la propiedad del dominio "llamados.cfp.addr.reverse" al registrador
	await ens.setSubnodeOwner(namehash.hash("addr.reverse"), keccak256("cfp"), accounts[0]);
	await ens.setSubnodeOwner(namehash.hash("cfp.addr.reverse"), keccak256("llamados"), ReverseRegistrarCalls.address);


	
	// Despliego un resolver distinto para las cuentas:
	await deployer.deploy(PublicResolverAccounts,ENSRegistry.address);
	let publicResolverAccounts = await PublicResolverAccounts.at(PublicResolverAccounts.address);

	// Creamos el dominio usuarios mediante el registrador
	await registrar.register(keccak256("usuarios"),accounts[0]);
	// Declaramos un resolver para este nuevo dominio usuarios;
	await ens.setResolver(namehash.hash("usuarios.cfp"), PublicResolverAccounts.address);
	
	// Para facilitar su uso, le damos un nombre
	await ens.setSubnodeOwner(namehash.hash("usuarios.cfp"), keccak256("resolver"), accounts[0]);
	// Declaramos el resolver de este nuevo dominio de primer nivel
	await ens.setResolver(namehash.hash("resolver.usuarios.cfp"), PublicResolverAccounts.address);
	// Registramos en el resolver su propia dirección asociada al nuevo dominio
	await publicResolverAccounts.setAddr(namehash.hash("resolver.usuarios.cfp"), PublicResolverAccounts.address);
	
	// Configurando el resolutor inverso para usuarios.cfp
	// Desplegamos el registrador reverso
	await deployer.deploy(ReverseRegistrarAccounts, ENSRegistry.address, PublicResolverAccounts.address);
	// Transferimos la propiedad del dominio "usuarios.cfp.addr.reverse" al registrador
	//await ens.setSubnodeOwner(namehash.hash("addr.reverse"), keccak256("cfp"), accounts[0]);
	await ens.setSubnodeOwner(namehash.hash("cfp.addr.reverse"), keccak256("usuarios"), ReverseRegistrarAccounts.address);

};
