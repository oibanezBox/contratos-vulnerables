//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;
//pragma solidity >=0.4.24;

interface ENS {

    // Logged when the owner of a node assigns a new owner to a subnode.
    event NewOwner(bytes32 indexed node, bytes32 indexed label, address own);

    // Logged when the owner of a node transfers ownership to a new account.
    event Transfer(bytes32 indexed node, address own);

    // Logged when the resolver for a node changes.
    event NewResolver(bytes32 indexed node, address resolv);

    // Logged when the TTL of a node changes
    event NewTTL(bytes32 indexed node, uint64 timetl);


    function setSubnodeOwner(bytes32 node, bytes32 label, address own) external;
    function setResolver(bytes32 node, address resolv) external;
    function setOwner(bytes32 node, address own) external;
    function setTTL(bytes32 node, uint64 timetl) external;
    function owner(bytes32 node) external view returns (address);
    function resolver(bytes32 node) external view returns (address);
    function ttl(bytes32 node) external view returns (uint64);

}
