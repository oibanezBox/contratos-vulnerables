//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;
//pragma solidity ^0.5.0;

import "./ENS.sol";

/**
 * The ENS registry contract.
 */
contract ENSRegistry is ENS {
    struct Record {
        address owner;
        address resolver;
        uint64 ttl;
    }

    mapping (bytes32 => Record) records;

    // Permits modifications only by the owner of the specified node.
    modifier only_owner(bytes32 node) {
        require(records[node].owner == msg.sender);
        _;
    }

    /**
     * @dev Constructs a new ENS registrar.
     */
    constructor() {
        records[0x0].owner = msg.sender;
    }

    /**
     * @dev Transfers ownership of a node to a new address. May only be called by the current owner of the node.
     * @param node The node to transfer ownership of.
     * @param own The address of the new owner.
     */
    function setOwner(bytes32 node, address own) external override only_owner(node) {
        emit Transfer(node, own);
        records[node].owner = own;
    }

    /**
     * @dev Transfers ownership of a subnode keccak256(node, label) to a new address. May only be called by the owner of the parent node.
     * @param node The parent node.
     * @param label The hash of the label specifying the subnode.
     * @param own The address of the new owner.
     */
    function setSubnodeOwner(bytes32 node, bytes32 label, address own) external override only_owner(node) {
        bytes32 subnode = keccak256(abi.encodePacked(node, label));
        emit NewOwner(node, label, own);
        records[subnode].owner = own;
    }

    /**
     * @dev Sets the resolver address for the specified node.
     * @param node The node to update.
     * @param resolv The address of the resolver.
     */
    function setResolver(bytes32 node, address resolv) external override only_owner(node) {
        emit NewResolver(node, resolv);   
        records[node].resolver = resolv;
    }

    /**
     * @dev Sets the TTL for the specified node.
     * @param node The node to update.
     * @param timetl The TTL in seconds.
     */
    function setTTL(bytes32 node, uint64 timetl) external override only_owner(node) {
        emit NewTTL(node, timetl);
        records[node].ttl = timetl;
    }

    /**
     * @dev Returns the address that owns the specified node.
     * @param node The specified node.
     * @return address of the owner.
     */
    function owner(bytes32 node) external override view returns (address) {
        return records[node].owner;
    }

    /**
     * @dev Returns the address of the resolver for the specified node.
     * @param node The specified node.
     * @return address of the resolver.
     */
    function resolver(bytes32 node) external override view returns (address) {
        return records[node].resolver;
    }

    /**
     * @dev Returns the TTL of a node, and any records associated with it.
     * @param node The specified node.
     * @return ttl of the node.
     */
    function ttl(bytes32 node) external override view returns (uint64) {
        return records[node].ttl;
    }

}
