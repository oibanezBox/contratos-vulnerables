//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "./ENS.sol";

abstract contract Resolver{
    function setName(bytes32 node, string memory name) public virtual;
    function setText(bytes32 node, string calldata key, string calldata value) public virtual;
}

contract ReverseRegistrarCalls {
    // namehash('llamados.cfp.addr.reverse')
    bytes32 public constant ADDR_REVERSE_NODE = 0x8e9223f8ce960a57e8a6169aec3ee06c7e97dc88c037f6659695fd9629c0bdd4;

    ENS public ens;
    Resolver public defaultResolver;

    /**
     * @dev Constructor
     * @param ensAddr The address of the ENS registry.
     * @param resolverAddr The address of the default reverse resolver.
     */
    constructor(ENS ensAddr, Resolver resolverAddr) {
        ens = ensAddr;
        defaultResolver = resolverAddr;

        // Assign ownership of the reverse record to our deployer
        ReverseRegistrarCalls oldRegistrar = ReverseRegistrarCalls(ens.owner(ADDR_REVERSE_NODE));
        if (address(oldRegistrar) != address(0x0)) {
            oldRegistrar.claim(msg.sender);
        }
    }
    
    /**
     * @dev Transfers ownership of the reverse ENS record associated with the
     *      calling account.
     * @param owner The address to set as the owner of the reverse record in ENS.
     * @return The ENS node hash of the reverse record.
     */
    function claim(address owner) public returns (bytes32) {
        return claimWithResolver(owner, address(0x0));
    }

    /**
     * @dev Transfers ownership of the reverse ENS record associated with the
     *      calling account.
     * @param owner The address to set as the owner of the reverse record in ENS.
     * @param resolver The address of the resolver to set; 0 to leave unchanged.
     * @return The ENS node hash of the reverse record.
     */
    function claimWithResolver(address owner, address resolver) public returns (bytes32) {
        bytes32 label = sha3HexAddress(msg.sender);
        bytes32 nod = keccak256(abi.encodePacked(ADDR_REVERSE_NODE, label));
        address currentOwner = ens.owner(nod);

        // Update the resolver if required
        if (resolver != address(0x0) && resolver != ens.resolver(nod)) {
            // Transfer the name to us first if it's not already
            if (currentOwner != address(this)) {
                ens.setSubnodeOwner(ADDR_REVERSE_NODE, label, address(this));
                currentOwner = address(this);
            }
            ens.setResolver(nod, resolver);
        }

        // Update the owner if required
        if (currentOwner != owner) {
            ens.setSubnodeOwner(ADDR_REVERSE_NODE, label, owner);
        }

        return nod;
    }


    function claimLabelWithResolver(address owner,bytes32 labelToAssign, address resolver) public returns (bytes32) {

        bytes32 nod = keccak256(abi.encodePacked(ADDR_REVERSE_NODE, labelToAssign));
        address currentOwner = ens.owner(nod);

        // Update the resolver if required
        if (resolver != address(0x0) && resolver != ens.resolver(nod)) {
            // Transfer the name to us first if it's not already
            if (currentOwner != address(this)) {
                ens.setSubnodeOwner(ADDR_REVERSE_NODE, labelToAssign, address(this));
                currentOwner = address(this);
            }
            ens.setResolver(nod, resolver);
        }

        // Update the owner if required
        if (currentOwner != owner) {
            ens.setSubnodeOwner(ADDR_REVERSE_NODE, labelToAssign, owner);
        }

        return nod;
    }

    /**
     * @dev Sets the `name()` record for the reverse ENS record associated with
     * the calling account. First updates the resolver to the default reverse
     * resolver if necessary.
     * @param name The name to set for this address.
     * @return The ENS node hash of the reverse record.
     */
    function setName(string memory name,bytes32 labelToAssign) public returns (bytes32) {
        bytes32 nod = claimLabelWithResolver(address(this), labelToAssign ,address(defaultResolver));
        defaultResolver.setName(nod, name);
        return nod;
    }

    function setText(bytes32 labelToAssign, string calldata key, string calldata value) public returns (bytes32) {
        bytes32 nod = claimLabelWithResolver(address(this), labelToAssign ,address(defaultResolver));
        defaultResolver.setText(nod, key,value);
        return nod;
    }

    /**
     * @dev Returns the node hash for a given account's reverse records.
     * @param addr The address to hash
     * @return The ENS node hash.
     */
    function node(address addr) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(ADDR_REVERSE_NODE, sha3HexAddress(addr)));
    }

     /**
     * @dev An optimised function to compute the sha3 of the lower-case
     *      hexadecimal representation of an Ethereum address.
     * @param addr The address to hash
     * @return ret The SHA3 hash of the lower-case hexadecimal encoding of the
     *         input address.
     */
    function sha3HexAddress(address addr) private pure returns (bytes32 ret) {
        addr;
        ret; // Stop warning us about unused variables
        assembly {
            let lookup := 0x3031323334353637383961626364656600000000000000000000000000000000

            for { let i := 40 } gt(i, 0) { } {
                i := sub(i, 1)
                mstore8(i, byte(and(addr, 0xf), lookup))
                addr := div(addr, 0x10)
                i := sub(i, 1)
                mstore8(i, byte(and(addr, 0xf), lookup))
                addr := div(addr, 0x10)
            }

            ret := keccak256(0, 40)
        }
    }
}
