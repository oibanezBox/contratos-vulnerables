import React, {useEffect, useState} from 'react'
import './App.css';
import NavBar from './components/NavBar';
import Login from './components/Login';
import { UseContent } from './hooks/UseContent';
import { CFP } from './components/CFP';
import { ListCreators } from './components/ListCreators';
import { ListCalls } from './components/ListCalls';
import { AdminPanel } from './components/AdminPanel';
import { MyAccount } from './components/MyAccount';
import { RegisterNewCall } from './components/RegisterNewCall';

window.$cfp = ""
window.$creator = ""
window.$callid = ""

function App() {
  

  const [theme, setTheme] = useState('dark');
  const [login, setLogin] = useState(false);
  const [content, handleChgCont] = UseContent();

  useEffect(() => {
    window.$theme = 'dark'
  }, [])

  const onChangeTheme = (value) => {
    value === 'dark'? setTheme('dark'): setTheme('light')
  }

  const onHandleClickLogin = (value) => {
    setLogin(value)
  }

  return (
    <div className="App">
      <header className={"App-header-"+theme}>
        <NavBar login={onHandleClickLogin} handleChgCont={handleChgCont} onChangeTheme={onChangeTheme}></NavBar>
        {content.creatorsTable && <ListCreators handleChgCont={handleChgCont}/>}
        {content.callsTable && <ListCalls handleChgCont={handleChgCont}/>}
        {content.cfp && <CFP/>}
        {content.account && <RegisterNewCall/>}
        {content.myaccount && <MyAccount/>}
        {content.adminPanel && <AdminPanel/>}
        {login? <Login theme={theme}/>:null }
      </header>
    </div>
  );
}

export default App;
