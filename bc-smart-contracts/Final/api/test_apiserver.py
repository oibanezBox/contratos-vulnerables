import requests, pytest
from os import urandom
from jsonschema import validate
from web3 import Web3
import eth_account
from dateutil.parser import isoparse
from dateutil.relativedelta import relativedelta
from dateutil.tz import gettz
from datetime import datetime
from random import randrange
import messages


calls_schema = {
    "type" : "object",
    "properties" : {
        "creator" : {"type" : "string"},
        "cfp": {"type": "string"},
    },
    "required": ["creator", "cfp"]
}

proposal_data_schema = {
    "type" : "object",
    "properties" : {
        "timestamp" : {"type" : "string"},
        "sender" : {"type" : "string"},
        "blockNumber": {
            "anyOf": [
                {"type": "number"},
                {"type": "string"}
            ]
        },
    },
    "required": ["sender", "blockNumber", "timestamp"]
}


def single_field_schema(field, field_type = "string"):
    return {
        "type" : "object",
        "properties" : {
            f"{field}" : {"type" : f"{field_type}"},
         },
        "required": [f"{field}"]
    }

message_schema = single_field_schema("message")
authorized_schema = single_field_schema("authorized", "boolean")
closing_time_schema = single_field_schema("closingTime")
address_schema = single_field_schema("address")

ART = gettz("GMT-3")
server = "http://localhost:5000"
application_json = "application/json"
accounts = []
calls = {}
contract_address = "undefined"
contract_owner = "undefined"

def url(action, arg = None):
    return f"{server}/{action}/{arg}" if arg else f"{server}/{action}"

def random_hex(length):
    return f"0x{urandom(length).hex()}"

def random_hash():
    return random_hex(32)

def random_address():
    return random_hex(20)

def random_signature():
    return random_hex(65)

def get_closing_time(past=False):
    days = 1 + randrange(90)
    if past:
        days = -days
    hour = randrange(8,18)
    return datetime.now(ART) + relativedelta(days=days, hour = hour, minute = 0, second = 0, microsecond = 0)


def sign(message, account):
    message = eth_account.messages.encode_defunct(hexstr=message)
    return account.sign_message(message).signature.hex()

def test_authorized_unknown_address():
    for _ in range(10):
        response = requests.get(url("authorized",random_address()))
        assert application_json in response.headers['Content-type']
        assert response.status_code == 200
        validate(instance=response.json(), schema=authorized_schema)
        assert response.json()["authorized"] == False

def test_authorized_invalid_address():
    for address in ["xx", "0", "0x", "0x0", random_address()[:-1], random_address()[:-2], random_hash()]:
        response = requests.get(url("authorized",address))
        assert application_json in response.headers['Content-type']
        assert response.status_code == 400
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"].startswith(messages.INVALID_ADDRESS)

def test_register():
    for _ in range(10):
        account = eth_account.Account.create()
        signature = sign(account.address, account)
        response = requests.post(url("register"), json={"address": account.address, "signature": signature})
        assert response.status_code == 200
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"] == messages.OK
        accounts.append(account)


def test_register_again():
    for account in accounts:
        signature = sign(account.address, account)
        response = requests.post(url("register"), json={"address": account.address, "signature": signature})
        assert response.status_code == 403
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"].startswith(messages.ALREADY_AUTHORIZED)

def test_register_invalid_address():
    signature = random_signature()
    for address in ["xx", "0", "0x", "0x0", random_address()[:-1], random_address()[:-2], random_hash()]:
        response = requests.post(url("register"), json={"address": address, "signature": signature})
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"].startswith(messages.INVALID_ADDRESS)
        assert response.status_code == 400

def test_register_invalid_signature():
    for account in accounts[1:]:
        signature = sign(account.address, accounts[0])
        response = requests.post(url("register"), json={"address": account.address, "signature": signature})
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"].startswith(messages.INVALID_SIGNATURE)
        assert response.status_code == 400

def test_authorized():
    for account in accounts:
        response = requests.get(url("authorized",account.address))
        assert application_json in response.headers['Content-type']
        assert response.status_code == 200
        validate(instance=response.json(), schema=authorized_schema)
        assert response.json()["authorized"]

def test_register_invalid_mimetype():
    account = eth_account.Account.create()
    signature = sign(account.address, account)
    response = requests.post(url("register"), data={"address": account.address, "signature": signature})
    assert application_json in response.headers['Content-type']
    validate(instance=response.json(), schema=message_schema)
    assert response.status_code == 400
    assert response.json()["message"].startswith(messages.INVALID_MIMETYPE)


def test_create_unauthorized():
    account = eth_account.Account.create()
    call_id = random_hash()
    signature = sign(call_id, account)
    closing_time = get_closing_time()
    response = requests.post(url("create"), json={"callId": call_id, "signature": signature, "closingTime": closing_time.isoformat()})
    assert application_json in response.headers['Content-type']
    validate(instance=response.json(), schema=message_schema)
    assert response.json()["message"].startswith(messages.UNAUTHORIZED)
    assert response.status_code == 403
     

def test_create():
    for account in accounts:
        call_id = random_hash()
        signature = sign(call_id, account)
        closing_time = get_closing_time()
        response = requests.post(url("create"), json={"callId": call_id, "signature": signature, "closingTime": closing_time.isoformat()})
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"] == messages.OK
        assert response.status_code == 201
        calls[call_id] = {"creator": account.address, "closingTime": closing_time}

def test_already_created():
    assert len(calls) > 0
    for call_id in calls.keys():
        signature = sign(call_id, accounts[0])
        closing_time = get_closing_time()
        response = requests.post(url("create"), json={"callId": call_id, "signature": signature, "closingTime": closing_time.isoformat()})
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"].startswith(messages.ALREADY_CREATED)
        assert response.status_code == 403

def test_created_data():
    assert len(calls) > 0
    for call_id, data in calls.items():
        response = requests.get(url("calls",call_id))
        assert application_json in response.headers['Content-type']
        assert response.status_code == 200
        validate(instance=response.json(), schema=calls_schema)
        assert response.json()["creator"] == data["creator"]
        calls[call_id]["cfp"] = response.json()["cfp"]

def test_created_closing_time():
    assert len(calls) > 0
    for call_id, data in calls.items():
        response = requests.get(url("closing-time", call_id))
        assert application_json in response.headers['Content-type']
        assert response.status_code == 200
        validate(instance=response.json(), schema=closing_time_schema)
        closing_time = isoparse(response.json()["closingTime"])
        assert  closing_time == data["closingTime"]



def test_contract_address():
    global contract_address
    response = requests.get(url("contract-address"))
    assert application_json in response.headers['Content-type']
    assert response.status_code == 200
    validate(instance=response.json(), schema=address_schema)
    contract_address = response.json()["address"]

def test_contract_owner():
    global contract_owner
    response = requests.get(url("contract-owner"))
    assert application_json in response.headers['Content-type']
    assert response.status_code == 200
    validate(instance=response.json(), schema=address_schema)
    contract_owner = response.json()["address"]

def test_register_proposal():
    assert len(calls) > 0
    for call_id in calls.keys():
        proposal = random_hash()
        response = requests.post(url("register-proposal"), json = {"callId": call_id, "proposal": proposal})
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=message_schema)
        assert response.json()["message"] == messages.OK
        assert response.status_code == 201
        response = requests.get(url("proposal-data",f"{call_id}/{proposal}"))
        assert application_json in response.headers['Content-type']
        validate(instance=response.json(), schema=proposal_data_schema)
        assert response.status_code == 200
        assert response.json()["sender"] == contract_owner
