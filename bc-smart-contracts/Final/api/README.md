# Trabajo Práctico 6
## Consigna
Implementar una API REST que interactúe con los contratos `CFP` y `CFPFactory` del práctico anterior y permita acceder a sus funcionalidades. 

En la API los *hashes*, direcciones y transacciones se expresarán como cadenas hexadecimales de la longitud adecuada y el prefijo "0x". Los valores enteros se expresarán como números decimales. La información temporal (por ejemplo, fecha y hora del cierre de convocatoria), deberá expresarse en formato ISO 8601.

El contrato `CFPFactory` debe estar desplegado en Ganache, la api debe conectarse con Ganache mediante http, y debe responder a requerimientos escuchando en el puerto 5000 de `localhost`.

La API deberá trabajar con una cuenta local, es decir, debe poder interactuar con un nodo que no tiene definida ninguna cuenta. Sin embargo, esta cuenta se obtendrá de la frase mnemónica que corresponda a la instancia Ganache en la que esté desplegado el contrato, y debe coincidir con la cuenta que desplegó el contrato.

El servidor debe recibir por línea de comandos o por un archivo de configuración la información necesaria para ser ejecutado. En ningún caso debe ser necesario modificar el código para que el servidor se ejecute.

La ABI y dirección de los contratos pueden obtenerse de los archivos creados por Truffle en el directorio `build` del proyecto.

La API debe proveer los siguientes *endpoints*:


### `/create`
* Crea un llamado a presentación de propuestas utilizando la función correspondiente de `CFPFactory`.
* Método: `POST`
* Content-type: `application/json`
* Cuerpo: Un objeto JSON con los siguientes campos:
    * `callId`: Hash que identifica al llamado.
    * `closingTime`: Fecha y hora del cierre de convocatoria, expresada en formato ISO 8601.
    * `signature`: Firma del `callId` con la clave privada del que hace la llamada. La API utiliza la dirección que se deriva de la firma para asignarle la propiedad de la propuesta creada.
* Retorno exitoso:
    * Código HTTP: 201
    * Cuerpo: Un objeto JSON con un campo "message" con valor OK.
* Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |Content-Type incorrecto   | 400    | INVALID_MIMETYPE     |
    |callId mal formado        | 400    | INVALID_CALLID       |
    |closingTime mal formado   | 400    | INVALID_TIME_FORMAT  |
    |callId ya existente       | 403    | ALREADY_CREATED      |
    |emisor no autorizado      | 403    | UNAUTHORIZED         |
    |tiempo de cierre inválido | 400    | INVALID_CLOSING_TIME |
    |firma inválida            | 400    | INVALID_SIGNATURE    |
    |desconocida               | 500    | INTERNAL_ERROR       |

### `/register`
* Permite a un usuario registrarse para crear llamados, y los autoriza de inmediato.
* Método: `POST`
* Content-type: `application/json`
* Cuerpo: Un objeto JSON con los siguientes campos:
    * `address`: Dirección del solicitante
    * `signature`: Firma de `address` con la clave privada del que hace la llamada. 
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con un campo "message" con valor OK.
* Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |Content-Type incorrecto   | 400    | INVALID_MIMETYPE     |
    |dirección inválida        | 400    | INVALID_ADDRESS      |
    |closingTime mal formado   | 400    | INVALID_TIME_FORMAT  |
    |ya estaba autorizado      | 403    | ALREADY_AUTHORIZED   |
    |firma inválida            | 400    | INVALID_SIGNATURE    |
    |desconocida               | 500    | INTERNAL_ERROR       |

### `/register-proposal`
* Permite a un usuario registrar una propuesta en un determinado llamado.
* Método: `POST`
* Content-type: `application/json`
* Cuerpo: Un objeto JSON con los siguientes campos:
    * `callId`: Hash que identifica al llamado.
    * `proposal`: Hash que identifica a la propuesta.
* Retorno exitoso:
    * Código HTTP: 201
    * Cuerpo: Un objeto JSON con un campo "message" con valor OK.
* Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |Content-Type incorrecto   | 400    | INVALID_MIMETYPE     |
    |callId mal formado        | 400    | INVALID_CALLID       |
    |callId inexistente        | 404    | CALLID_NOT_FOUND     |
    |propuesta mal formada     | 400    | INVALID_PROPOSAL     |
    |propuesta ya existente    | 403    | ALREADY_REGISTERED   |
    |desconocida               | 500    | INTERNAL_ERROR       |

### `/authorized/:address`

* Método: `GET`
* Argumento: `:address` corresponde a una dirección
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con un campo booleano 
        * "authorized", el estado de autorización de la dirección provista.
* Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |dirección inválida        | 400    | INVALID_ADDRESS      |
    |desconocida               | 500    | INTERNAL_ERROR       |


### `/calls/:call_id`

* Método: `GET`
* Argumento: `:call_id` es el hash que identifica a un llamado
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con dos campos de tipo `string`:
        * "closingTime", la fecha y hora de cierre del llamado, en formato ISO 8601.
 * Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |callId mal formado        | 400    | INVALID_CALLID       |
    |callId inexistente        | 404    | CALLID_NOT_FOUND     |
    |desconocida               | 500    | INTERNAL_ERROR       |


### `/closing-time/:call_id`

* Método: `GET`
* Argumento: `:call_id` es el hash que identifica a un llamado
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con un campo de tipo `string`:
        * "creator", la dirección del creador del llamado.
        * "cfp", la dirección del contrato que representa al llamado.
* Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |callId mal formado        | 400    | INVALID_CALLID       |
    |callId inexistente        | 404    | CALLID_NOT_FOUND     |
    |desconocida               | 500    | INTERNAL_ERROR       |


### `/contract-address`

* Método: `GET`
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con un campo de tipo `string`:
        * "address", dirección del contrato factoría.

### `/contract-owner`

* Método: `GET`
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con un campo de tipo `string`:
        * "address", dirección del dueño contrato factoría.


## `/proposal-data/:call_id/:proposal`

* Método: `GET`
* Argumentos: 
    * `:call_id` es el hash que identifica a un llamado.
    * `:proposal` es el hash que identifica una propuesta.
* Retorno exitoso:
    * Código HTTP: 200
    * Cuerpo: Un objeto JSON con tres campos:
        * "sender", de tipo "string" con la dirección del que envió la propuesta.
        * "blockNumber", de tipo "number" con el número de bloque en el cual se registró.
        * "timestamp", de tipo string, con la fecha y hora de registro en formato ISO 8601.
 * Retorno fallido:
    * Código HTTP: Según la tabla siguiente.
    * Cuerpo: Un objeto JSON con un campo "message" con valor indicado en la tabla siguiente.

    | Causa                    | Código |  Mensaje             |
    |--------------------------|--------|----------------------|
    |callId mal formado        | 400    | INVALID_CALLID       |
    |callId inexistente        | 404    | CALLID_NOT_FOUND     |
    |propuesta mal formada     | 400    | INVALID_PROPOSAL     |
    |propuesta inexistente     | 404    | PROPOSAL_NOT_FOUND   |
    |desconocida               | 500    | INTERNAL_ERROR       |


## Mensajes 
| ID                  | Mensaje                              | 
|---------------------|--------------------------------------|
|INVALID_ADDRESS      | "Dirección inválida"                 |
|INVALID_SIGNATURE    | "Firma inválida"                     |
|INVALID_MIMETYPE     | "Tipo MIME inválido"                 |
|INVALID_CALLID       | "Identificador de llamado incorrecto"|
|INVALID_PROPOSAL     | "Formato de propuesta incorrecto"    |
|INVALID_TIME_FORMAT  | "Formato de tiempo incorrecto"       |
|INVALID_CLOSING_TIME | "Tiempo de cierre inválido"          |
|ALREADY_AUTHORIZED   | "Ya está autorizado"                 |
|ALREADY_CREATED      | "El llamado ya existe"               |
|ALREADY_REGISTERED   | "La propuesta ya ha sido registrada" |
|CALLID_NOT_FOUND     | "El llamado no existe"               |
|PROPOSAL_NOT_FOUND   | "La propuesta no existe"             |
|UNAUTHORIZED         | "No autorizado"                      |
|INTERNAL_ERROR       | "Error interno"                      |
|OK                   | "OK"                                 |

