#!/usr/bin/env python3
import constants
from flask import Flask, request, make_response, json, jsonify
import re
import io
import os
from web3 import Web3
from web3.middleware import geth_poa_middleware
from eth_account.messages import encode_defunct
from business import CFPBusiness
from sys import argv, stderr, exit
from flask_cors import CORS
app = Flask(__name__)
#CORS(app, resources=r'/*', headers='Content-Type')
CORS(app)
DEFAULT_WEB3_URI = "http://localhost:7545"
DEFAULT_CONTRACT_LOCATION = "../5/build/contracts"


@app.route("/create", methods=["POST"])
def create():
    if request.mimetype == "application/json":
        req = request.json
        response = business.create(req.get("callId"),req.get("closingTime") ,req.get("signature"))
    else:
        response = jsonify(message = constants.INVALID_MIMETYPE)
        response.status_code = 400
    return response

@app.route("/register", methods=["POST"])
def register():
    if request.mimetype == "application/json":
        req = request.json
        response = business.register(req.get("address"),req.get("signature"))
    else:
        response = jsonify(message = constants.INVALID_MIMETYPE)
        response.status_code = 400
    return response

@app.route("/register-proposal", methods=["POST"])
def registerProposal():
    if request.mimetype == "application/json":
        req = request.json
        response = business.registerProposal(req.get("callId"),req.get("proposal"))
    else:
        response = jsonify(message = constants.INVALID_MIMETYPE)
        response.status_code = 400
    return response

@app.route("/authorized/<address>", methods=["GET"])
def isAuthorized(address):
    response = business.isAuthorized(address)
    return response


@app.route("/calls/<callid>", methods=["GET"])
def calls(callid):
    response = business.calls(callid)
    return response
    
@app.route("/creators", methods=["GET"])
def creators():
    response = business.creators()
    return response

@app.route("/creators/<creator>", methods=["GET"])
def callsFromCreator(creator):
    response = business.callsFromCreator(creator)
    return response

@app.route("/closing-time/<call_id>", methods=["GET"])
def closingTime(call_id):
    response = business.closingTime(call_id)
    return response

@app.route("/contract-address", methods=["GET"])
def contractAddress():
    response = business.contractAddress()
    return response

@app.route("/contract-owner", methods=["GET"])
def contractOwner():
    response = business.contractOwner()
    return response

@app.route("/proposal-data/<callid>/<proposal>", methods=["GET"])
def proposalData(callid,proposal):
    response = business.proposalData(callid,proposal)
    return response

if __name__ == '__main__':
    import argparse
    try: 
        parser = argparse.ArgumentParser()
        parser.add_argument("--uri", help=f"Uri de la red eth",default=DEFAULT_WEB3_URI)
        parser.add_argument("-p","--mnemonic-file", help=f"Path del archivo que contiene la mnemónica para la generación de cuentas")
        parser.add_argument("-n","--network-id", help="Network id de la red ethereum", type=int,default=5777)
        parser.add_argument("-c","--config-dir", help=f"Path del directorio que contiene la información de los contratos",default=DEFAULT_CONTRACT_LOCATION)
        args = parser.parse_args()
        if(args.uri == None):
                raise Exception('Error: Specify the uri to make the connection with --uri parameter')
        else:
                if(args.uri.split("/")[0]== 'http:' or args.uri.split("/")[0]== 'https:' ):
                    w3 = Web3(Web3.HTTPProvider(args.uri))
                else:
                    w3 = Web3(Web3.IPCProvider(args.uri))
        if(w3.isConnected() == False):
            raise Exception('Could not connect to node using provided uri')
        w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        if( args.mnemonic_file != None ):
            try:
                with open(args.mnemonic_file) as f:
                    mnemonic = str(f.read()).strip()
            except (FileNotFoundError) as e:
                print("The password file was not found in the specified path",file =stderr )
                exit(1)
        else:
            mnemonic =  input("Please enter the mnemonic: ")
        jsonObject = {}
        #Chequear si el ultimo carater de configDire es una /
        with open(args.config_dir+"/CFPFactory.json") as f:
            jsonObj = json.load( f )
            contract =  w3.eth.contract( abi = jsonObj['abi'], address = jsonObj['networks'][str(args.network_id)]['address'] )
        with open(args.config_dir+"/CFP.json") as f:    
            jsonObj = json.load( f )
            abi =  jsonObj['abi']
        business = CFPBusiness(w3,contract,mnemonic,abi)
        app.run(debug=True)
    except (FileNotFoundError) as e:
        print("No file could be found in directory: ",file =stderr )
        exit(1)
    except KeyError as e:
        print(f"No se encuentra un contrato desplegado en la red ",e,file=stderr)
        exit(1)