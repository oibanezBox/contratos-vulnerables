//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.3;

import "./BadCoinGame.sol";

contract GameLauncher{
    mapping(address => Game[]) public games;


    function launchBadCoinGame() public payable {
        uint bet = msg.value;
        games[msg.sender].push( new BadCoinGame{value: bet}());
    }

    function gameCount(address creator) public view returns (uint){
        return games[creator].length;
    }

}