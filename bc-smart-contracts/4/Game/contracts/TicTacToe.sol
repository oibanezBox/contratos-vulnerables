//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.3;

import "./Game.sol";


contract TicTacToe is Game {
    event Play(address player, uint256 row, uint256 col);

    uint[3][3] board;
    address public chairperson;
    bool win;
    uint turnCount;

    constructor() payable Game(){
        chairperson = msg.sender;
        for(uint i = 0; i < 3; i++){
            for(uint k = 0; k < 3; k++){
                board[i][k] = 2; 
            }
        }
        turnCount = 0;
        setInitialPlayer();
        // constructor
    }

    // Permite realizar una jugada, indicando fila y columna
    // Las filas y columnas comienzan en 0
    // Sólo puede invocarse si el juego ya comenzó
    // Sólo puede invocarla el jugador que tiene el turno
    // Si la jugada es inválida, se rechaza con error "invalid move"
    // Debe emitir el evento Play(player, row, col) con cada jugada
    function play(uint256 row, uint256 col) public onlyRunning inTurn isValidIndex(row,col){
        require(board[row][col] == 2, "invalid move");
        board[row][col] = playerIndex(msg.sender);
        turnCount += 1;
        emit Play(msg.sender,row, col);
        if(playerWin(row,col) == true){
            winner = players[playerIndex(msg.sender)];
            winnings[playerIndex(msg.sender)] = 2*bet;
            transferWinnings(winner);
            status = Status.ended;
            emit Winner(winner);
        }else{
            // si llegamos a nueve turnos y ninguno gano --> empate
            if(turnCount == 9){
                //logica de empate
                emit Draw(players[0], players[1]);
                winnings[0] = bet;
                winnings[1] = bet;
                transferWinnings(players[playerIndex(msg.sender)]);
                status = Status.ended;
            }else{
                // Si no cambiamos de turno
                changeTurn();
            } 
        }
        
    }

    function playerWin(uint row, uint col) internal view returns (bool){

        //chequea por fila, mueve solo las columnas, si una casilla ya no es igual a player index no hizo linea
        for(uint i = 0;i < 3; i++){
            if(board[row][i]!= playerIndex(msg.sender)){
                break;
            }
            if(i == 2){
                return true;
            }
        }
        //chequea por columna, mueve solo las filas, si una casilla ya no es igual a player index no hizo linea
        for(uint i = 0;i < 3; i++){
            if(board[i][col]!= playerIndex(msg.sender)){
                break;
            }
            if(i == 2){
                return true;
            }
        }

        //chequeamos la antidiagonal --> si observamos los indices de la misma veremos que 
        //en cualquiera de sus 3 casillas, la suma de los indices (col + row) es igual a 
        //la longitud de una de sus dimensiones (n) menos uno, en este caso 2
        if( col + row == 2 ){
            for(uint i = 0;i < 3; i++){
                if(board[i][2-i] != playerIndex(msg.sender)){
                    break;
                }
                if(i == 2 ){
                    return true;
                }
            }
        }

        // por último chequeamos la diagonal, si observamos sus indices nos daremos cuenta
        // que la col es igual a la row, para sus 3 casillas
        if( col == row){
            for(uint i = 0;i < 3; i++){
                if(board[i][i] != playerIndex(msg.sender)){
                    break;
                }
                if(i == 2 ){
                    return true;
                }
            }
        }
        // Finalmente si no hizo ninguna linea o diagonal, no ganó
        return false;
    }

    modifier isValidIndex(uint row,uint col) {
        require(col < 3, "invalid index of column out of board limit");
        require(row < 3, "invalid index of row out of board limit");
        _;
    }
}
