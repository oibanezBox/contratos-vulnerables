#!/usr/bin/env python3

from web3.auto import w3
from web3.middleware import geth_poa_middleware
w3.middleware_onion.inject(geth_poa_middleware, layer=0)

print(w3.eth.blockNumber)