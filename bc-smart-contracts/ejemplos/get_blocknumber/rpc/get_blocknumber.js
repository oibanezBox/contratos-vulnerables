#!/usr/bin/env node

var request = require('request');

function rpcreq( opname, params, callback )
        {
            var     extra       =   params.join(',');
            var     body        =   JSON.parse("{"+
                '"jsonrpc":"2.0",' +
                '"id":1,' +
                '"method":"' + opname + '",' +
                '"params":[' + extra + ']'
                +"}"
            );
            request.post({
                uri:            'http://localhost:8545',
                json:           true,
                body:           body,
                callback:       function RPCresponse( err, obj )
                {
                    if ( err )
                        throw new Error( err );
                    if ( obj.body.error && obj.body.error.code && obj.body.error.message )
                        throw new Error( 'Error ' + obj.body.error.code + ": "+ obj.body.error.message );
                    callback(obj.body.result);
                }
            });
        };


rpcreq("eth_blockNumber",[], number => console.log(parseInt(number, 16)));
