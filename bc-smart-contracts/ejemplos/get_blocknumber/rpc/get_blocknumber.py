#!/usr/bin/env python3

rpc_port = 8545
rpc_host = "localhost"
import requests


def rpcreq(method, params):
    session = requests.Session()
    payload = {"jsonrpc":"2.0", "method":method,"params":params,"id":1}
    headers = {'Content-type':'application/json'} 
    response = session.post(
        "http://{}:{}".format(rpc_host,rpc_port),
        json=payload,
        headers=headers)
    return response.json()['result']


def get_blockNumber():
    return rpcreq("eth_blockNumber",[])

if __name__ == "__main__":
        print(int(get_blockNumber(),16))
