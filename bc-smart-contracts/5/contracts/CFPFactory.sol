//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "./CFP.sol";

contract CFPFactory {
    // Evento que se emite cuando se crea un llamado a presentación de propuestas
    event CFPCreated(address creator, bytes32 callId, CFP cfp);

    // Estructura que representa un llamado
    struct CallForProposals {
        address creator;
        CFP cfp;
    }

    struct RegisteredAccounts{
        bool isEnabled;
        bool isPending;
        uint index;
    }

    address public owner;

    constructor() {
        owner = msg.sender;
    }

    // Dirección del dueño de la factoría
    //function owner() public view returns (address) {}

    mapping(bytes32 => CallForProposals) public calls;
    // Devuelve el llamado asociado con un callId
    //function calls(bytes32 callId)
    //    public
    //    view
    //    returns (CallForProposals memory)
    //{}
    mapping(address => RegisteredAccounts) public registeredAccounts;
    mapping(address => bytes32[]) public callsFromCreator;
    address[] public pendingRegistrations;
    address[] public creators;

    // Devuelve la dirección de un creador de la lista de creadores
    //function creators(uint index) public view returns (address) {
    //    return creators[index];
    //}

    /** Crea un llamado, con un identificador y un tiempo de cierre
     *  Si ya existe un llamado con ese identificador, revierte con el mensaje de error "El llamado ya existe"
     *  Si el emisor no está autorizado a crear llamados, revierte con el mensaje "No autorizado"
     */
    function create(bytes32 callId, uint256 timestamp) public returns (CFP cfp) {
        return _createFor(callId, timestamp, msg.sender);
    }


    function createFor(bytes32 callId, uint timestamp, address creator) public onlyCreator returns (CFP cfp){
        return _createFor(callId, timestamp, creator);
    }

    function _createFor(bytes32 callId, uint timestamp, address creator) internal returns (CFP cfp){
        require(calls[callId].creator == address(0), "El llamado ya existe");
        require(registeredAccounts[creator].isEnabled == true, "No autorizado");
        cfp = new CFP(callId, timestamp);
        if(callsFromCreator[creator].length == 0){
            creators.push(creator);
        }
        callsFromCreator[creator].push(callId);
        calls[callId].creator = creator;
        calls[callId].cfp = cfp; 
        emit CFPCreated(creator, callId, cfp);
    }

    // Devuelve la cantidad de cuentas que han creado llamados.
    function creatorsCount() public view returns (uint256) {
        return creators.length;
    }

   

    /// Devuelve el identificador del llamado que está en la posición `index` de la lista de llamados creados por `creator`
    function createdBy(address creator, uint256 index)
        public
        view
        returns (bytes32)
    {
        return callsFromCreator[creator][index];
    }

    // Devuelve la cantidad de llamados creados por `creator`
    function createdByCount(address creator) public view returns (uint256) {
        return callsFromCreator[creator].length;
    }

    /** Permite a un usuario registrar una propuesta, para un llamado con identificador `callId`.
     *  Si el llamado no existe, revierte con el mensaje  "El llamado no existe".
     *  Registra la propuesta en el llamado asociado con `callId` y pasa como creador la dirección del emisor del mensaje.
     */
    function registerProposal(bytes32 callId, bytes32 proposal) public {
        require(calls[callId].creator != address(0), "El llamado no existe");
        calls[callId].cfp.registerProposalFor(proposal,msg.sender);
    }

    /** Permite que una cuenta se registre para poder crear llamados.
     *  El registro queda en estado pendiente hasta que el dueño de la factoría lo autorice.
     *  Si ya se ha registrado, revierte con el mensaje "Ya se ha registrado"
     */
    function register() public{
        require(!registeredAccounts[msg.sender].isPending, "Ya se ha registrado");
        registeredAccounts[msg.sender].isEnabled = false;
        registeredAccounts[msg.sender].isPending = true;
        registeredAccounts[msg.sender].index = pendingRegistrations.length;
        pendingRegistrations.push(msg.sender);
    }

    /** Autoriza a una cuenta a crear llamados.
     *  Sólo puede ser ejecutada por el dueño de la factoría.
     *  En caso contrario revierte con el mensaje "Solo el creador puede hacer esta llamada".
     *  Si la cuenta se ha registrado y está pendiente, la quita de la lista de pendientes.
     */
    function authorize(address creator) public onlyCreator{
        if(registeredAccounts[creator].isPending == true){
            registeredAccounts[creator].isEnabled = true;
            registeredAccounts[creator].isPending= false;
            deleteEntry(registeredAccounts[creator].index);
        }else{
            registeredAccounts[creator].isEnabled = true;
        }
    }

    /** Quita la autorización de una cuenta para crear llamados.
     *  Sólo puede ser ejecutada por el dueño de la factoría.
     *  En caso contrario revierte con el mensaje "Solo el creador puede hacer esta llamada".
     *  Si la cuenta se ha registrado y está pendiente, la quita de la lista de pendientes.
     */
    function unauthorize(address creator) public onlyCreator{
        if(registeredAccounts[creator].isPending == true){
            registeredAccounts[creator].isEnabled = false;
            registeredAccounts[creator].isPending= false;
            deleteEntry(registeredAccounts[creator].index);
        }else{
            registeredAccounts[creator].isEnabled = false;
        }
    }

    // Devuelve la lista de todas las registraciones pendientes.
    // Sólo puede ser ejecutada por el dueño de la factoría
    // En caso contrario revierte con el mensaje "Solo el creador puede hacer esta llamada".
    function getAllPending() public view onlyCreator returns (address[] memory){
        return pendingRegistrations;
    }

    // Devuelve la registración pendiente con índice `index`
    // Sólo puede ser ejecutada por el dueño de la factoría
    // En caso contrario revierte con el mensaje "Solo el creador puede hacer esta llamada".
    function getPending(uint256 index) public view onlyCreator returns (address){
        return pendingRegistrations[index];
    }

    // Devuelve la cantidad de registraciones pendientes.
    // Sólo puede ser ejecutada por el dueño de la factoría
    // En caso contrario revierte con el mensaje "Solo el creador puede hacer esta llamada".
    function pendingCount() public view onlyCreator returns (uint256) {
        return pendingRegistrations.length;
    }

    // Devuelve verdadero si una cuenta se ha registrado, tanto si su estado es pendiente como si ya se la ha autorizado.
    function isRegistered(address account) public view returns (bool) {
        return registeredAccounts[account].isPending || registeredAccounts[account].isEnabled;
    }

    // Devuelve verdadero si una cuenta está autorizada a crear llamados.
    function isAuthorized(address account) public view returns (bool) {
        return registeredAccounts[account].isEnabled;
    }

    modifier onlyCreator() {
    require(
        msg.sender == owner,
        "Solo el creador puede hacer esta llamada"
    );
    _;
    }
    function deleteEntry(uint index) internal{
            if(index < pendingRegistrations.length - 1){
                pendingRegistrations[index] = pendingRegistrations[ pendingRegistrations.length - 1];
            }
            pendingRegistrations.pop();
    }
}
