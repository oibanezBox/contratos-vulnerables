//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract Subasta {
    address liderActual;
    uint ofertaMasAlta;

    address creador;
    uint fechaCierre;
    bool subastaActiva = true;

    constructor(uint256 _fechaCierre){
        creador = msg.sender;
        fechaCierre = _fechaCierre;
    }

    function ofertar() payable public {
        require(msg.value > ofertaMasAlta, "El monto ingresado no supera la mejor oferta actual!");
        require(fechaCierre > block.timestamp,"Subasta cerrada");
        require(subastaActiva == true, "El creador cerro la subasta");
        require(payable(liderActual).send(ofertaMasAlta));

        liderActual = msg.sender;
        ofertaMasAlta = msg.value;
    }


    function terminarSubasta() public soloCreador {
        subastaActiva = false;
    }


    modifier soloCreador() {
        require(
            msg.sender == creador,
            "Solo el creador puede hacer esta llamada"
        );
        _;
    }
}