//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract Subasta {
    address liderActual;
    uint ofertaMasAlta;

    address creador;
    uint fechaCierre;
    bool subastaActiva = true;
    address[] private cuentasReembolso;
    mapping (address => uint) public reembolsos;

    constructor(uint256 _fechaCierre){
        creador = msg.sender;
        fechaCierre = _fechaCierre;
    }

    function ofertar() payable public {
        require(msg.value > ofertaMasAlta, "El monto ingresado no supera la mejor oferta actual!");
        require(fechaCierre > block.timestamp,"Subasta cerrada");
        require(subastaActiva == true, "El creador cerro la subasta");

        cuentasReembolso.push(liderActual);
        reembolsos[liderActual] = ofertaMasAlta;

        liderActual = msg.sender;
        ofertaMasAlta = msg.value;        
    }



    function reembolsarATodos() public soloCreador {
        for(uint x; x < cuentasReembolso.length; x++) { 
            require(
                payable(cuentasReembolso[x]).send(reembolsos[cuentasReembolso[x]]));
        }
    }

    function terminarSubasta() public soloCreador {
        subastaActiva = false;
    }


    modifier soloCreador() {
        require(
            msg.sender == creador,
            "Solo el creador puede hacer esta llamada"
        );
        _;
    }
}