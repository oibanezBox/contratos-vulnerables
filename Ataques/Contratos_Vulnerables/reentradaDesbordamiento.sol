//SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract ReentradaVul{

    mapping(address  => uint) public balanceDeCuenta;

    address public owner;
    
    constructor(){
        owner = msg.sender;
    }

    function retirarBalance() public {
        uint montoAExtraer = balanceDeCuenta[msg.sender];
        (bool exito, ) = msg.sender.call{value:montoAExtraer}(""); 
        require(exito, "Error al transferir");
        balanceDeCuenta[msg.sender] = 0;
        payable(msg.sender).transfer(montoAExtraer);
    }

    function transferir(address para, uint256 monto) public {
        /* Chequeo que el que envia la transacción cuente con saldo */
        require(balanceDeCuenta[msg.sender] >= monto);
        /* Añadir y quitar balance en las cuentas correspondientes */
        balanceDeCuenta[msg.sender] -= monto;
        balanceDeCuenta[para] += monto;
    }

    function depositar() external payable{
        require(msg.value > 0,"Es necesario un monto positivo");
        balanceDeCuenta[msg.sender] = msg.value;
    }

    function retirarMonto(uint monto) payable public{
        require(msg.value > 0,"Es necesario un monto positivo");
        require(balanceDeCuenta[msg.sender] >= monto,"Saldo insuficiente");
        balanceDeCuenta[msg.sender] -= monto;
        payable(msg.sender).transfer(monto);
    }

}